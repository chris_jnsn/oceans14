#include "ShaderOceanSimulationSinus.h"
#include "Wave.h"

ShaderOceanSimulationSinus::ShaderOceanSimulationSinus(GLuint shader_mask, const char** shaderPaths) : CVK::ShaderMinimal(shader_mask,shaderPaths)
{
	m_oceanWidthID  = glGetUniformLocation( m_ProgramID, "oceanWidth");
	m_oceanHeightID = glGetUniformLocation( m_ProgramID, "oceanHeight");
    m_timeStepID = glGetUniformLocation(m_ProgramID, "timeStep");
}

void ShaderOceanSimulationSinus::update(unsigned int const & oceanWidth, unsigned int const & oceanHeight, float const & timeStep, std::vector<Wave*> const & waves)
{
	CVK::ShaderMinimal::update();

	glUniform1i( m_oceanWidthID, oceanWidth);
	glUniform1i( m_oceanHeightID, oceanHeight);
    glUniform1f(m_timeStepID, timeStep);

    for (unsigned int i = 0; i < waves.size(); i++)
    {
        glUniform3fv(glGetUniformLocation(m_ProgramID, std::string("waves[" + std::to_string(i) + "].direction").c_str()), 1, glm::value_ptr(waves.at(i)->getDirection()));

        glUniform1f(glGetUniformLocation(m_ProgramID, std::string("waves[" + std::to_string(i) + "].amplitude").c_str()),       waves.at(i)->getAmplitude());
        glUniform1f(glGetUniformLocation(m_ProgramID, std::string("waves[" + std::to_string(i) + "].waveLength").c_str()),      waves.at(i)->getWaveLength());
        glUniform1f(glGetUniformLocation(m_ProgramID, std::string("waves[" + std::to_string(i) + "].parameterK").c_str()),      waves.at(i)->getParameterK());
        glUniform1f(glGetUniformLocation(m_ProgramID, std::string("waves[" + std::to_string(i) + "].frequency").c_str()),       waves.at(i)->getFrequency());
        glUniform1f(glGetUniformLocation(m_ProgramID, std::string("waves[" + std::to_string(i) + "].phaseVelocity").c_str()),   waves.at(i)->getPhaseVelocity());
        glUniform1f(glGetUniformLocation(m_ProgramID, std::string("waves[" + std::to_string(i) + "].phaseShift").c_str()),      waves.at(i)->getPhaseShift());
    }


}