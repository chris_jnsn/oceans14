#version 330 

layout (location = 0) in vec4 Position;
layout (location = 2) in vec2 Tcoord;

out vec2 passUV;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

void main()
{
  gl_Position = modelMatrix * Position;
  passUV = Tcoord;
}