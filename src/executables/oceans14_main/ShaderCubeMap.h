#pragma once

#include "CVK_2/CVK_Defs.h"
#include "CVK_2/CVK_Camera.h"
#include "CVK_2/CVK_Light.h"
#include "CVK_2/CVK_Material.h"
#include "CVK_2/CVK_Geometry.h"
#include "CVK_2/CVK_ShaderMinimal.h"

class ShaderCubeMap : public CVK::ShaderMinimal
{
public:
	ShaderCubeMap( GLuint shader_mask, const char** shaderPaths, CVK::CubeMapTexture *cubeMap);
	void update( );

	inline void setCubeMapTexture(CVK::CubeMapTexture *cubeMap) {m_cubeMapTexture = cubeMap;}

	CVK::CubeMapTexture * getCubeMapTexture() {return m_cubeMapTexture;}

private:
	CVK::CubeMapTexture *m_cubeMapTexture;
	GLuint m_cubeMapTextureID;
};
