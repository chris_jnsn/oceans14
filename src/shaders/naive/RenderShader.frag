#version 330 core

in vec3 passPosition;
in vec3 passNormal;
in vec2 passTcoord;

uniform mat4 viewMatrix;

uniform struct
{
	float kd, ks, kt;
    vec3 diffColor;
	vec3 specColor;
    float shininess;
} mat;

uniform struct LIGHT
{
  vec4 pos;    //pos.w = 0 dir. light, pos.w = 1 point light
  vec3 col;
  vec3 spot_direction;
  float spot_exponent;
  float spot_cutoff;  // no spotlight if cutoff = 0
} light[8]; //MAX_LIGHTS

uniform struct FogParams
{
   vec3 col; // Fog color
   float start; // This is only for linear fog
   float end; // This is only for linear fog
   float density; // For exp and exp2 equation
   int mode; // 1 = linear, 2 = exp, 3 = exp2
} fog;

uniform int numLights;

uniform vec3 lightAmbient;

uniform int useColorTexture;
uniform sampler2D colorTexture;

out vec4 fragmentColor;

float getFogFactor(FogParams fog, float z)
{
   float f = 0.0;
   if (fog.mode == 1)
      f = (fog.end-z)/(fog.end-fog.start);
   else if (fog.mode == 2)
      f = exp(-fog.density*z);
   else if( fog.mode == 3)
      f = exp(-pow(fog.density*z, 2.0));

   f = clamp(f, 0.0, 1.0);

   return f;
}

void main()
{
	vec3 diffuse_color;
	float diffuse_alpha;

	// diffuse color
	if (useColorTexture != 0)
	{
        diffuse_color = texture( colorTexture, passTcoord ).rgb;
        //diffuse_alpha = texture( colorTexture, passTcoord ).a;
		diffuse_alpha = 1.f;
    }

	else
	{
        diffuse_color = mat.diffColor;
        diffuse_alpha = 1.f;
    }

	//ambient light
	fragmentColor = vec4(diffuse_color,diffuse_alpha);
}
