#pragma once

#include <CVK_2/CVK_Framework.h>

class ShaderNoiseGenerator;
class ShaderMapGenerator;

class Noise
{
public:
	Noise(unsigned int const & noiseMapWidth, unsigned int const & noiseMapHeight);
    ~Noise();

    void update(float const & timeStep);

    inline CVK::FBO & getNoiseMapFBO() {return *m_noisemapFBO;}
	inline CVK::FBO & getNormalMapFBO() { return *m_normalmapFBO; }

protected:

	void initShader();

private:

    CVK::FBO *m_noisemapFBO;
	CVK::FBO *m_normalmapFBO;

	CVK::Ortho *m_sfqOrtho;
	CVK::Trackball *m_sfqCamera;
	CVK::Node *m_sfqNode;
	CVK::Plane *m_sfqPlane;

	ShaderNoiseGenerator *m_noiseShader;
	ShaderMapGenerator *m_normalShader;
};