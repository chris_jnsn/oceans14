#include "ShaderOceanSimulationGerstner.h"
#include "Wave.h"
#include <array>

ShaderOceanSimulationGerstner::ShaderOceanSimulationGerstner(GLuint shader_mask, const char** shaderPaths, CVK::CubeMapTexture *cubeMap) : CVK::ShaderMinimal(shader_mask,shaderPaths)
{
    m_oceanWidthID = glGetUniformLocation(m_ProgramID, "oceanWidth");
    m_oceanDepthID = glGetUniformLocation(m_ProgramID, "oceanDepth");

    m_timeStepID = glGetUniformLocation(m_ProgramID, "timeStep");

    m_noiseTextureID = glGetUniformLocation(m_ProgramID, "noiseMap");
    m_noiseMapSizeID = glGetUniformLocation(m_ProgramID, "noiseMapSize");

    m_foamTextureID = glGetUniformLocation(m_ProgramID, "foamTexture");

    m_bottomLeftPointID = glGetUniformLocation(m_ProgramID, "bottomLeftPoint");
    m_bottomRightPointID = glGetUniformLocation(m_ProgramID, "bottomRightPoint");
    m_topLeftPointID = glGetUniformLocation(m_ProgramID, "topLeftPoint");
    m_topRightPointID = glGetUniformLocation(m_ProgramID, "topRightPoint");

    m_eyePosWsID = glGetUniformLocation(m_ProgramID, "eyePosWS");

	m_lightPosWsID = glGetUniformLocation(m_ProgramID, "lightPos");

	m_maxAmplitudeID = glGetUniformLocation(m_ProgramID, "maxAmp");

    m_shineFactorID = glGetUniformLocation(m_ProgramID, "shineFactor");

    m_foamIntensityID = glGetUniformLocation(m_ProgramID, "foamIntensity");

    m_cubeMapTexture = cubeMap;
    m_cubeMapTextureID = glGetUniformLocation(m_ProgramID, "cubeMap");
}

void ShaderOceanSimulationGerstner::update(float const & oceanWidth, float const & oceanDepth, float const & timeStep, std::vector<Wave*> const & waves, GLuint const & noiseMap, float const & noiseMapWidth, float const & noiseMapHeight, CVK::Texture & foamTexture, float const & foamIntensity, std::array<glm::vec4, 4> const & xzRangePoints, glm::vec3 const & camPosition, float const & shininess, glm::vec3 & lightPos, float const & maxAmplitude)
{
	CVK::ShaderMinimal::update();


    glUniform1f(m_timeStepID, timeStep);
    glUniform1f(m_oceanWidthID, oceanWidth);
    glUniform1f(m_oceanDepthID, oceanDepth);

    glUniform2fv(m_noiseMapSizeID, 1, glm::value_ptr(glm::vec2(noiseMapWidth, noiseMapHeight)));

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, noiseMap);
	glUniform1i(m_noiseTextureID, 1);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, foamTexture.getTexture());
    glUniform1i(m_foamTextureID, 2);

    if (m_cubeMapTexture)
    {
        glActiveTexture(CUBE_MAP_TEXTURE_UNIT);
        glUniform1i(m_cubeMapTextureID, 3);
    }

    glUniform4fv(m_bottomLeftPointID, 1, glm::value_ptr(xzRangePoints[0]));
    glUniform4fv(m_bottomRightPointID, 1, glm::value_ptr(xzRangePoints[1]));
    glUniform4fv(m_topLeftPointID, 1, glm::value_ptr(xzRangePoints[2]));
    glUniform4fv(m_topRightPointID, 1, glm::value_ptr(xzRangePoints[3]));

    glUniform3fv(m_eyePosWsID, 1, glm::value_ptr(camPosition));

	glUniform3fv(m_lightPosWsID, 1, glm::value_ptr(lightPos));

    glUniform1f(m_shineFactorID, shininess);

	glUniform1f(m_maxAmplitudeID, maxAmplitude);

    glUniform1f(m_foamIntensityID, foamIntensity);

    for (unsigned int i = 0; i < waves.size(); i++)
    {
        glUniform3fv(glGetUniformLocation(m_ProgramID, std::string("waves[" + std::to_string(i) + "].direction").c_str()), 1, glm::value_ptr(waves.at(i)->getDirection()));

        glUniform1f(glGetUniformLocation(m_ProgramID, std::string("waves[" + std::to_string(i) + "].amplitude").c_str()),       waves.at(i)->getAmplitude());
        glUniform1f(glGetUniformLocation(m_ProgramID, std::string("waves[" + std::to_string(i) + "].waveLength").c_str()),      waves.at(i)->getWaveLength());
        glUniform1f(glGetUniformLocation(m_ProgramID, std::string("waves[" + std::to_string(i) + "].parameterK").c_str()),      waves.at(i)->getParameterK());
        glUniform1f(glGetUniformLocation(m_ProgramID, std::string("waves[" + std::to_string(i) + "].frequency").c_str()),       waves.at(i)->getFrequency());
        glUniform1f(glGetUniformLocation(m_ProgramID, std::string("waves[" + std::to_string(i) + "].phaseVelocity").c_str()),   waves.at(i)->getPhaseVelocity());
        glUniform1f(glGetUniformLocation(m_ProgramID, std::string("waves[" + std::to_string(i) + "].phaseShift").c_str()),      waves.at(i)->getPhaseShift());
    }	


}