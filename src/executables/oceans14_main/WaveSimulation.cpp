#include "WaveSimulation.h"

#include <iostream>
#include "Wave.h"
#include "ShaderOceanSimulationSinus.h"
#include "ShaderOceanSimulationGerstner.h"
#include "ShaderSimpleTexture.h"
#include "GridProjector.h"
#include "OceanCamera.h"

//IMPORTANT: keep this const value in sync with the corresponding const value in the waveSimulation Shader!
const unsigned int MAX_NUMBER_OF_WAVES = 60;

CVK::Texture *g_noiseTexture;

WaveSimulation::WaveSimulation(
    SIMULATION_TYPE simulationType,
    unsigned int const & oceanMapWidth, unsigned int const & oceanMapHeight,
    unsigned int const & oceanMeshWidth, unsigned int const & oceanMeshDepth,
    float const & oceanBaseHeight, 
    OceanCamera * oceanCam, CVK::CubeMapTexture * cubeMap) :
    m_oceanMapWidth(oceanMapWidth),
    m_oceanMapHeight(oceanMapHeight),
    m_oceanMeshWidth(oceanMeshWidth),
    m_oceanMeshDepth(oceanMeshDepth),
	m_simulationType(simulationType),
	m_waves(new std::vector<Wave*>()),
	m_waveSimulationNode(nullptr),
    m_oceanGeometry(nullptr),
    m_oceanSimulationSinusShader(nullptr),
	m_oceanSimulationGerstnerShader(nullptr),
    m_heightmapFBO(nullptr),
    m_sfqCamera(nullptr),
    m_sfqNode(nullptr),
    m_sfqPlane(nullptr),
    m_sfqOrtho(new CVK::Ortho(0.0f, 1.0f, 0.0f, 1.0f, 0.1f, 100.0f)),
    m_oceanBaseHeight(oceanBaseHeight),
    m_maxAmplitude(0)
{
	if(oceanMapWidth < 5)
	{
		std::cout << "Error in WaveSimulation Constructor - unreasonably small ocean width parameter - correcting to 10, which is the minimum" << std::endl;
		m_oceanMapWidth = 10;
	}

	if(oceanMapHeight < 5)
	{
		std::cout << "Error in WaveSimulation Constructor - unreasonably small ocean height parameter - correcting to 10, which is the minimum" << std::endl;
		m_oceanMapHeight = 10;
	}

	initWaveGeometry();
	initShader(cubeMap);

    m_heightmapFBO = new CVK::FBO(m_oceanMapWidth, m_oceanMapHeight, 3);

    m_sfqCamera = new CVK::Trackball(m_oceanMapWidth,m_oceanMapHeight,m_sfqOrtho);

    m_sfqPlane = new CVK::Plane();
    m_sfqPlane->set_Points(
        glm::vec3(-1.f, 1.f, 0.f),
        glm::vec3(-1.f, -1.f, 0.f),
        glm::vec3(1.f, -1.f, 0.f),
        glm::vec3(1.f, 1.f, 0.f));

    m_sfqPlane->set_Tcoords(
        glm::vec2(0.f, 1.f),
        glm::vec2(0.f, 0.f),
        glm::vec2(1.f, 0.f),
        glm::vec2(1.f, 1.f)
        );

    m_sfqNode = new CVK::Node("sfq");
    m_sfqNode->setGeometry(m_sfqPlane);

    m_gridProjector = new GridProjector(oceanCam);
}

WaveSimulation::~WaveSimulation()
{
    for (auto it = m_waves->begin(); it != m_waves->end(); ++it)
        if (*it) delete *it;
    
    delete m_waves;
    delete m_waveSimulationNode;
    delete m_oceanGeometry;
    delete m_oceanSimulationSinusShader;
    delete m_oceanSimulationGerstnerShader;
    delete m_heightmapFBO;
    delete m_sfqCamera;
    delete m_sfqNode;
    delete m_sfqPlane;
    delete m_gridProjector;
}

void WaveSimulation::addWaveToSimulation(Wave * newWave)
{
    if (m_waves->size() > MAX_NUMBER_OF_WAVES)
    {
        std::cout << "Error in WaveSimulation::addWaveToSimulation - reached maximum number of waves, which is " << MAX_NUMBER_OF_WAVES << "!" << std::endl;
        return;
    }

    if(newWave->getAmplitude() > m_maxAmplitude)
        m_maxAmplitude = newWave->getAmplitude();

    m_waves->push_back(newWave);
}

bool WaveSimulation::updateSimulation()
{
    m_maxAmplitude = 0;
    for(int i = 0; i < m_waves->size();i++)
    {
        if(m_waves->at(i)->getAmplitude() > m_maxAmplitude)
            m_maxAmplitude = m_waves->at(i)->getAmplitude();
    }

    //UPDATE GRID:
    bool oceanVisible = m_gridProjector->updateGrid(*m_oceanGeometry, m_maxAmplitude, m_oceanBaseHeight);


    if (!oceanVisible)
        return false;
                
    return true;
}

void WaveSimulation::render(bool const & wireFrame, float const & timeStep, GLuint const & noiseMap, CVK::Texture & foamTexture, float const & foamIntensity, OceanCamera & oceanCam, float const & shininess, glm::vec3 & lightPos, float const & maxAmplitude)
{
    CVK::State::getInstance()->setShader(m_oceanSimulationGerstnerShader);
    m_oceanSimulationGerstnerShader->update(m_oceanMeshWidth, m_oceanMeshDepth, timeStep, *m_waves, noiseMap, m_oceanMapWidth,m_oceanMapHeight, foamTexture, foamIntensity, m_gridProjector->getXZRangePoints(), oceanCam.getCamPos(), shininess, lightPos, maxAmplitude);

    /*
    oceanShader.update(
        g_waveSimulation->getHeightMapFBO(),
        g_waveSimulation->getGridProjector().getXZRangePoints(),
        &g_oceanCam->getCamPos(),
        g_showNormalsMode, g_showReflectionsMode, g_showSunMode, g_showFoamMode,
        shininess, foamIntensity);
    */


    if (!wireFrame)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    m_waveSimulationNode->render();



    //UPDATE HEIGHTMAP:
    //glDisable(GL_DEPTH_TEST);
    //glDisable(GL_BLEND);

    //CVK::State::getInstance()->setCamera(m_sfqCamera);
    //m_heightmapFBO->bind();
    //glClear(GL_COLOR_BUFFER_BIT);
    /*
    if (m_simulationType == SIN_WAVES)
    {
        CVK::State::getInstance()->setShader(m_oceanSimulationSinusShader);
        m_oceanSimulationSinusShader->update(m_oceanMapWidth, m_oceanMapHeight, simulationTimeStep, *m_waves);
    }
    
    else if (m_simulationType == GERSTNER_WAVES)
    {
   
       
    }
    */
    //m_sfqNode->render();
    //m_heightmapFBO->unbind();
}


GridProjector & WaveSimulation::getGridProjector()
{
    return *m_gridProjector;
}

void WaveSimulation::initWaveGeometry()
{
	if(m_waveSimulationNode)
        delete m_waveSimulationNode;

    if(m_oceanGeometry)
        delete m_oceanGeometry;

    m_waveSimulationNode = new CVK::Node("oceanScene");

    m_waveSimulationNode->setMaterial(new CVK::Material(TEXTURES_PATH "/blue.bmp", 1.0f, 0.75f, white, 1000000.0f));

    //Create geometry======================================================
    CVK::Geometry *oceanGeometry = new CVK::Geometry();

    std::vector<glm::vec4> *tmpVert1        = oceanGeometry->getVertices();
    std::vector<glm::vec3> *tmpNormal1      = oceanGeometry->getNormals();
    std::vector<glm::vec2> *tmpUV1          = oceanGeometry->getUVs();
    std::vector<unsigned int> *tmpIndex1    = oceanGeometry->getIndex();

    for(unsigned int z = 1; z <= m_oceanMeshDepth; z++)
    {
        for(unsigned int x = 1; x <= m_oceanMeshWidth; x++)
        {
            tmpVert1->push_back(glm::fvec4(
                    (static_cast<float>(x) / static_cast<float>(m_oceanMeshWidth)),
                    0.f,
                    (static_cast<float>(z) / static_cast<float>(m_oceanMeshDepth)),
                    1.0f
                ));
            
            tmpNormal1->push_back(glm::fvec3(0.0f, 1.0f, 0.0f));

            float curU = static_cast<float>(x) / static_cast<float>(m_oceanMeshWidth);
            float curV = static_cast<float>(z) / static_cast<float>(m_oceanMeshDepth);
            
            tmpUV1->push_back(glm::fvec2(curU, curV));

        }   
    } 

    //add indices
    for (int z = 0; z < m_oceanMeshDepth - 1; z++)
     {
        for (int x = 0; x < m_oceanMeshWidth - 1; x++) 
        {
            //face1
            tmpIndex1->push_back(z * (m_oceanMeshWidth) + x);
            tmpIndex1->push_back(z * (m_oceanMeshWidth) + (x + 1));
            tmpIndex1->push_back((z + 1) * (m_oceanMeshWidth) + x);
                            
            //face2
            tmpIndex1->push_back((z + 1) * (m_oceanMeshWidth) + x);
            tmpIndex1->push_back(z * (m_oceanMeshWidth) + (x + 1));
            tmpIndex1->push_back((z + 1) * (m_oceanMeshWidth) + (x + 1));
        }
    }

    oceanGeometry->createBuffers();

    m_waveSimulationNode->setGeometry(oceanGeometry);
    //========================================================================
}

void WaveSimulation::initShader(CVK::CubeMapTexture * cubeMap)
{
	//temporary
	//g_noiseTexture = new CVK::Texture(TEXTURES_PATH "/normalmap.png");

	const char *shaderNamesSinus[2] = { SHADERS_PATH "/OceanSimulation/OceanSimulationSinus.vert", SHADERS_PATH "/OceanSimulation/OceanSimulationSinus.frag" };

	m_oceanSimulationSinusShader = new ShaderOceanSimulationSinus(VERTEX_SHADER_BIT | FRAGMENT_SHADER_BIT, shaderNamesSinus);

    const char *shaderNamesGerstner[2] = { SHADERS_PATH "/OceanSimulation/OceanSimulationGerstner.vert", SHADERS_PATH "/OceanSimulation/OceanSimulationGerstner.frag" };

    m_oceanSimulationGerstnerShader = new ShaderOceanSimulationGerstner(VERTEX_SHADER_BIT | FRAGMENT_SHADER_BIT, shaderNamesGerstner, cubeMap);

    const char *shaderNamesDebug[2] = { SHADERS_PATH "/screenFilling/screenFill.vert", SHADERS_PATH "/screenFilling/simpleTexture.frag" };
    m_shaderSimpleTexture = new ShaderSimpleTexture(VERTEX_SHADER_BIT | FRAGMENT_SHADER_BIT, shaderNamesDebug);
}
