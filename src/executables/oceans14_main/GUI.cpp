#include "GUI.h"
#include <imgui.cpp>
#include <GL/glew.h>
#include "WaveSimulation.h"
#include "Wave.h"

static int          shader_handle, vert_handle, frag_handle;
static int          texture_location, proj_mtx_location;
static int          position_location, uv_location, colour_location;
static size_t       vbo_max_size = 20000;
static unsigned int vbo_handle, vao_handle;
static bool         mousePressed[2] = { false, false };

ImVec4              clear_col = ImColor(114, 144, 154);

#define OFFSETOF(TYPE, ELEMENT) ((size_t)&(((TYPE *)0)->ELEMENT))

const ImVec4 darkerGrey         = ImVec4(0.18f, 0.18f, 0.18f, 1.0f);        //Elements like Headers, Buttons and Stuff
const ImVec4 darkerGreyHovered  = ImVec4(0.19f, 0.19f, 0.19f, 1.0f);
const ImVec4 darkGrey           = ImVec4(0.22f, 0.22f, 0.22f, 1.0f);        //Mostly for Background
const ImVec4 midGrey            = ImVec4(0.35f, 0.35f, 0.35f, 1.0f);        //Just for a few certain Elements (Scrollbar etc.)
const ImVec4 midGreyHovered     = ImVec4(0.4f, 0.4f, 0.4f, 1.0f);

const ImVec4 whiteCol           = ImVec4(0.9f, 0.9f, 0.9f, 1.0f);           //Text Color
const ImVec4 blueHightlight     = ImVec4(0.5960f, 0.0666f, 0.0666f,1.0f);              //HighlightElements and Grabbed Things like sliders

const ImVec4 qualityGreen       = ImVec4(0.0f, 1.0f, 0.0f, 1.0f);           //quality measurement colors for FPS etc..
const ImVec4 qualityYellow      = ImVec4(1.0f, 1.0f, 0.0f, 1.0f);
const ImVec4 qualityRed         = ImVec4(1.0f, 0.0f, 0.0f, 1.0f);

GUI::GUI(unsigned int const & guiWidth, unsigned int const & guiHeight) :
  m_visible(true),
  m_initialized(false),
    m_width(guiWidth),
    m_height(guiHeight),
  m_renderTextA(std::string()), m_renderTextB(std::string()), m_renderTextC(std::string()),
  m_finalFPS(nullptr),
  m_waveSimulation(nullptr)
{
  m_headerTexture = new CVK::Texture(TEXTURES_PATH "/guiHeader.png");
}

GUI::~GUI()
{
  if (vao_handle) glDeleteVertexArrays(1, &vao_handle);
  if (vbo_handle) glDeleteBuffers(1, &vbo_handle);
  glDetachShader(shader_handle, vert_handle);
  glDetachShader(shader_handle, frag_handle);
  glDeleteShader(vert_handle);
  glDeleteShader(frag_handle);
  glDeleteProgram(shader_handle);

  ImGui::Shutdown();

  delete m_headerTexture;
}

void GUI::show()
{
  m_visible = true;
}

void GUI::hide()
{
  m_visible = false;
}


void GUI::render(GLFWwindow* window)
{
  if (!m_initialized)
    initGUI();

  if (!m_visible)
  {
    return;
  }

  ImGuiIO& io = ImGui::GetIO();

  mousePressed[0] = static_cast<bool>(glfwGetMouseButton(window,GLFW_MOUSE_BUTTON_1));
  mousePressed[1] = static_cast<bool>(glfwGetMouseButton(window,GLFW_MOUSE_BUTTON_2));

  update(window);

  bool t = true;

  static bool no_titlebar   = true;
  static bool no_resize     = true;
  static bool no_move       = true;
  static bool no_scrollbar  = false;
  static bool no_collapse   = false;

  //static float bg_alpha = 1.0f;
  static float bg_alpha = 0.65f;

  ImGuiWindowFlags  window_flags = 0;
  if (no_titlebar)  window_flags |= ImGuiWindowFlags_NoTitleBar;
  if (no_resize)    window_flags |= ImGuiWindowFlags_NoResize;
  if (no_move)      window_flags |= ImGuiWindowFlags_NoMove;
  if (no_scrollbar) window_flags |= ImGuiWindowFlags_NoScrollbar;
  if (no_collapse)  window_flags |= ImGuiWindowFlags_NoCollapse;


  int width;
  int height;

  glfwGetWindowSize(window, &width, &height);

  ImGui::SetNextWindowPos(ImVec2(static_cast<float>(width - m_width), 0.0f));
  ImGui::SetNextWindowSize(ImVec2(m_width, static_cast<float>(m_height)));

  if(!ImGui::Begin(" ", &t, ImVec2(0, 0), bg_alpha, window_flags))
    ImGui::End();
  else
  {
    drawElements(window);
    ImGui::End();
  }

  glViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);

  ImGui::Render();
}

void GUI::setFPSVariable(int *finalFPS, double *renderTime)
{
  m_finalFPS = finalFPS;
  m_renderTime = renderTime;
}

void GUI::setWireframeVariable(bool *wire)
{
  m_wireframe = wire;
}

void GUI::setExternalCameraVariable(bool *externalCam)
{
    m_useExternalCamera = externalCam;
}

void GUI::setShowNormalsVariable(bool *norm)
{
    m_showNormals = norm;
}


void GUI::setWaveSimulation(WaveSimulation * waveSim)
{
    m_waveSimulation = waveSim;
}

void GUI::setShowReflectionsVariable(bool *reflect)
{
	m_showReflections = reflect;
}

void GUI::setShowSunVariable(bool *sun)
{
	m_showSun = sun;
}

void GUI::setShowFoamVariable(bool *foam)
{
	m_showFoam = foam;
}

void GUI::setShininessVariable(float *shineFactor)
{
	m_shininess = shineFactor;
}

void GUI::setFoamItensityVariable(float *foamItensity)
{
	m_foamIntensitiy = foamItensity;
}

void GUI::setOceanRoughnessVariable(float *roughness)
{
  m_oceanRoughness = roughness;
}

void GUI::drawElements(GLFWwindow* window)
{
  ImTextureID tex_id = ImTextureID(m_headerTexture->getTexture());
  ImGui::Image(tex_id, ImVec2(250, 70), ImVec2(0, 1), ImVec2(1, 0), ImColor(255, 255, 255, 255), ImColor(0, 0, 0, 0));

  if (m_renderTextA.size() == 0)
  {
    m_renderTextA = "OpenGL-Version: ";
    m_renderTextB = "GLSL-Version: ";
    m_renderTextC = "GPU: ";

    m_renderTextA.append(reinterpret_cast<char const *>(glGetString(GL_VERSION)));
    m_renderTextB.append(reinterpret_cast<char const *>(glGetString(GL_SHADING_LANGUAGE_VERSION)));
    m_renderTextC.append(reinterpret_cast<char const *>(glGetString(GL_RENDERER)));

    //Cut off after last zero of version number
    if (m_renderTextA.find("0 ") != std::string::npos)
      m_renderTextA = m_renderTextA.substr(0, (m_renderTextA.find("0 ")) + 1);
    if (m_renderTextB.find("") != std::string::npos)
      m_renderTextB = m_renderTextB.substr(0, (m_renderTextB.find("0 ")) + 1);

  }

  if (ImGui::CollapsingHeader("Performance"))
  {
      static ImVector<float> values; if (values.empty()) { values.resize(100); memset(&values.front(), 0, values.size()*sizeof(float)); }
      static size_t values_offset = 0;

      //RENDERER INFO
      ImGui::Spacing();
      ImGui::Spacing();
      tab(); ImGui::Text(m_renderTextA.c_str());
      tab(); ImGui::Text(m_renderTextB.c_str());
      tab(); ImGui::Text(m_renderTextC.c_str());

      if (!m_finalFPS)
        std::cout << "WARNING in GUI::drawInfoHeader - missing GUI::setFPSVariable call()" << std::endl;

      else
      {
        ImGui::Spacing();
        ImGui::Spacing();
        ImGui::Separator();
        ImGui::Spacing();
        ImGui::Spacing();

        ImVec4 finalHightlightColor;

        if (*m_finalFPS >= 50)
        {
          finalHightlightColor = qualityGreen;
        }

        else if (*m_finalFPS >= 30 && *m_finalFPS < 50)
        {
          finalHightlightColor = qualityYellow;
        }

        else
        {
          finalHightlightColor = qualityRed;
        }

        ImGui::Spacing();
        ImGui::Columns(2);
        tab(); ImGui::Text("Rendering"); ImGui::NextColumn();  ImGui::Text(doubleDigitToString(*m_renderTime).c_str());  ImGui::SameLine(); ImGui::Text("ms"); ImGui::NextColumn();
        tab(); ImGui::Text("FPS:"); ImGui::NextColumn(); ImGui::TextColored(finalHightlightColor, std::to_string(*m_finalFPS).c_str()); ImGui::NextColumn();
        ImGui::Columns(1);
        ImGui::Spacing();
        ImGui::Spacing();
        values[values_offset] = static_cast<float>(*m_finalFPS) / 60.0f;
        values_offset = (values_offset + 1) % values.size();
        tab(); ImGui::PlotLines(" ", &values.front(), (int)values.size(), (int)values_offset, "", 0.0f, 1.1f, ImVec2(220, 40));
        ImGui::Spacing();
        ImGui::Spacing();
      }
    }

  if (ImGui::CollapsingHeader("Simulation"))
  {
      ImGui::Spacing();
      ImGui::Spacing();
      tab(); ImGui::Checkbox("Projected Grid Camera", m_useExternalCamera);
      //ImGui::Spacing();
      //ImGui::Spacing();

      //SIMULATION_TYPE currentSimulationTypeIndex = m_waveSimulation->getWaveSimulationType();
      //const char* simulationTypeNames[] = { "SINUS", "GERSTNER" };
      //tab(); ImGui::Text("Simulation Type: ");
      //tab(); ImGui::Combo("##Simulation Type", reinterpret_cast<GLint*>(&currentSimulationTypeIndex), simulationTypeNames, ((int)(sizeof(simulationTypeNames) / sizeof(*simulationTypeNames))));

      //if (currentSimulationTypeIndex != m_waveSimulation->getWaveSimulationType() && currentSimulationTypeIndex < NUMBER_OF_SIMULATION_TYPES)
      //    m_waveSimulation->setSimulationType(currentSimulationTypeIndex);

      ImGui::Spacing();
      ImGui::Spacing();

      float oldRoughness = *m_oceanRoughness; 
      tab(); ImGui::Text("Ocean Roughness");     
      tab(); ImGui::SliderFloat("##Roughness", m_oceanRoughness, 0.0f, 0.38f);
         
      if(oldRoughness != *m_oceanRoughness)
      {
        float change = *m_oceanRoughness - oldRoughness;

        for (unsigned int i = 0; i < m_waveSimulation->getWaves().size(); i++)
        {
          m_waveSimulation->getWaves().at(i)->setAmplitude(
            m_waveSimulation->getWaves().at(i)->getAmplitude() + change
            );
        }

      }     
      
      /*
      //for (unsigned int i = 0; i < m_waveSimulation->getWaves().size(); i++)
      for (unsigned int i = 0; i < 2; i++)
      {
          tab();
          if (ImGui::CollapsingHeader(std::string("Wave " + std::to_string(i+1)).c_str()))
          {
              ImGui::Spacing();

              glm::vec3 currentDir = m_waveSimulation->getWaves().at(i)->getDirection();
              float directionXZ[2] = { currentDir.x, currentDir.z };
              tab(); tab(); ImGui::DragFloat2(std::string("Dir. ##" + std::to_string(i)).c_str(), directionXZ, 0.1f, -1.0f, 1.0f);
              m_waveSimulation->getWaves().at(i)->setDirection(glm::vec3(directionXZ[0], 0.0, directionXZ[1]));

              float amplitude = m_waveSimulation->getWaves().at(i)->getAmplitude();
              tab(); tab(); ImGui::SliderFloat(std::string("Ampl. ##" + std::to_string(i)).c_str(), &amplitude, 0.0f, 5.0f);
              if (amplitude != m_waveSimulation->getWaves().at(i)->getAmplitude())
                  m_waveSimulation->getWaves().at(i)->setAmplitude(amplitude);

              float waveLength = m_waveSimulation->getWaves().at(i)->getWaveLength();
              tab(); tab(); ImGui::SliderFloat(std::string("Length ##" + std::to_string(i)).c_str(), &waveLength, 0.1f, 20.0f);
              if (waveLength != m_waveSimulation->getWaves().at(i)->getWaveLength())
                  m_waveSimulation->getWaves().at(i)->setWaveLength(waveLength);
              
              float phaseShift = m_waveSimulation->getWaves().at(i)->getPhaseShift();
              tab(); tab(); ImGui::SliderFloat(std::string("Shift ##" + std::to_string(i)).c_str(), &phaseShift, 0.0f, 5.0f);
              if (phaseShift != m_waveSimulation->getWaves().at(i)->getPhaseShift())
                  m_waveSimulation->getWaves().at(i)->setPhaseShift(phaseShift);

              ImGui::Spacing();
              ImGui::Spacing();
              ImGui::Spacing();
          }
      }
      */
      

      ImGui::Spacing();
      ImGui::Spacing();
  }

  if (ImGui::CollapsingHeader("Visualization"))
  {
    ImGui::Spacing();
    ImGui::Spacing();
    tab(); ImGui::Text("Wireframe");
    tab(); ImGui::Checkbox("##Wireframe", m_wireframe);
    //tab(); ImGui::Checkbox("Show Normals", m_showNormals);
	//tab(); ImGui::Checkbox("Show Reflection", m_showReflections);
	//tab(); ImGui::Checkbox("Show Sun", m_showSun);
    tab(); ImGui::Text("Shininess");

	tab(); ImGui::SliderFloat("##Shininess", m_shininess, 1.0f, 100.0f);
	//tab(); ImGui::Checkbox("Show Foam", m_showFoam);
    tab(); ImGui::Text("Foam Intensity");

  tab(); ImGui::SliderFloat("##Foamitensity", m_foamIntensitiy, 0.0f, 0.2f);
    ImGui::Spacing();
    ImGui::Spacing();
  }
}

void GUI::tab()
{
  ImGui::Text("");
  ImGui::SameLine();
}

std::string GUI::doubleDigitToString(double& number)
{
  std::string returnVal = std::to_string(number);

  if (returnVal.find(".") == 1)
    return std::string("  ").append(returnVal);

  return returnVal;
}

void GUI::update(GLFWwindow* window)
{
  ImGuiIO& io = ImGui::GetIO();
  // Setup resolution (every frame to accommodate for window resizing)
  int w;
  int h;
  glfwGetWindowSize(window, &w, &h);

  int display_w, display_h;
  glfwGetFramebufferSize(window, &display_w, &display_h);
  io.DisplaySize = ImVec2((float)display_w, (float)display_h);                                   // Display size, in pixels. For clamping windows positions.

  // Setup time step
  static double time = 0.0f;
  const double current_time = glfwGetTime();
  io.DeltaTime = (float)(current_time - time);
  time = current_time;

  // Setup inputs
  // (we already got mouse wheel, keyboard keys & characters from glfw callbacks polled in glfwPollEvents())
  double mouse_x;
  double mouse_y;

  glfwGetCursorPos(window, &mouse_x, &mouse_y);

  mouse_x *= (float)display_w / w;                                                               // Convert mouse coordinates to pixels
  mouse_y *= (float)display_h / h;
  io.MousePos = ImVec2((float)mouse_x, (float)mouse_y);                                          // Mouse position, in pixels (set to -1,-1 if no mouse / on another screen, etc.)
  io.MouseDown[0] = mousePressed[0] || glfwGetMouseButton(window,GLFW_MOUSE_BUTTON_LEFT) != 0;  // If a mouse press event came, always pass it as "mouse held this frame", so we don't miss click-release events that are shorter than 1 frame.
  io.MouseDown[1] = mousePressed[1] || glfwGetMouseButton(window,GLFW_MOUSE_BUTTON_RIGHT) != 0;

  // Start the frame
  ImGui::NewFrame();
}

static void renderfn(ImDrawList** cmd_lists, int cmd_lists_count)
{
  if (cmd_lists_count == 0)
    return;

  // Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled
  glEnable(GL_BLEND);
  glBlendEquation(GL_FUNC_ADD);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_CULL_FACE);
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_SCISSOR_TEST);
  glActiveTexture(GL_TEXTURE0);

  //Setup orthographic projection matrix
  const float width = ImGui::GetIO().DisplaySize.x;
  const float height = ImGui::GetIO().DisplaySize.y;

  const float ortho_projection[4][4] =
  {
    { 2.0f / width, 0.0f, 0.0f, 0.0f },
    { 0.0f, 2.0f / -height, 0.0f, 0.0f },
    { 0.0f, 0.0f, -1.0f, 0.0f },
    { -1.0f, 1.0f, 0.0f, 1.0f },
  };
  glUseProgram(shader_handle);
  glUniform1i(texture_location, 0);
  glUniformMatrix4fv(proj_mtx_location, 1, GL_FALSE, &ortho_projection[0][0]);


  // Grow our buffer according to what we need
  size_t total_vtx_count = 0;

  for (int n = 0; n < cmd_lists_count; n++)
    total_vtx_count += cmd_lists[n]->vtx_buffer.size();


  glBindBuffer(GL_ARRAY_BUFFER, vbo_handle);
  size_t neededBufferSize = total_vtx_count * sizeof(ImDrawVert);
  if (neededBufferSize > vbo_max_size)
  {
    vbo_max_size = neededBufferSize + 5000;  // Grow buffer
    glBufferData(GL_ARRAY_BUFFER, vbo_max_size, NULL, GL_STREAM_DRAW);
  }

  // Copy and convert all vertices into a single contiguous buffer
  unsigned char* buffer_data = (unsigned char*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

  if (!buffer_data)
    return;

  for (int n = 0; n < cmd_lists_count; n++)
  {
    const ImDrawList* cmd_list = cmd_lists[n];
    memcpy(buffer_data, &cmd_list->vtx_buffer[0], cmd_list->vtx_buffer.size() * sizeof(ImDrawVert));
    buffer_data += cmd_list->vtx_buffer.size() * sizeof(ImDrawVert);
  }

  glUnmapBuffer(GL_ARRAY_BUFFER);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(vao_handle);

  int cmd_offset = 0;
  for (int n = 0; n < cmd_lists_count; n++)
  {
    const ImDrawList* cmd_list = cmd_lists[n];
    int vtx_offset = cmd_offset;
    const ImDrawCmd* pcmd_end = cmd_list->commands.end();

    for (const ImDrawCmd* pcmd = cmd_list->commands.begin(); pcmd != pcmd_end; pcmd++)
    {
      glBindTexture(GL_TEXTURE_2D, (GLuint)(intptr_t)pcmd->texture_id);
      glScissor((int)pcmd->clip_rect.x, (int)(height - pcmd->clip_rect.w), (int)(pcmd->clip_rect.z - pcmd->clip_rect.x), (int)(pcmd->clip_rect.w - pcmd->clip_rect.y));
      glDrawArrays(GL_TRIANGLES, vtx_offset, pcmd->vtx_count);
      vtx_offset += pcmd->vtx_count;
    }
    cmd_offset = vtx_offset;
  }

  // Restore modified state
  glBindVertexArray(0);
  glUseProgram(0);
  glDisable(GL_SCISSOR_TEST);
  glBindTexture(GL_TEXTURE_2D, 0);

  glDisable(GL_BLEND);
  //glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);

}

void GUI::initGUI()
{
  m_initialized = true;

  const GLchar *vertex_shader =
    "#version 330\n"
    "uniform mat4 ProjMtx;\n"
    "in vec2 Position;\n"
    "in vec2 UV;\n"
    "in vec4 Color;\n"
    "out vec2 Frag_UV;\n"
    "out vec4 Frag_Color;\n"
    "void main()\n"
    "{\n"
    "	Frag_UV = UV;\n"
    "	Frag_Color = Color;\n"
    "	gl_Position = ProjMtx * vec4(Position.xy,0,1);\n"
    "}\n";

  const GLchar* fragment_shader =
    "#version 330\n"
    "uniform sampler2D Texture;\n"
    "in vec2 Frag_UV;\n"
    "in vec4 Frag_Color;\n"
    "out vec4 Out_Color;\n"
    "void main()\n"
    "{\n"
    "	Out_Color = Frag_Color * texture( Texture, Frag_UV.st);\n"
    "}\n";

  shader_handle = glCreateProgram();
  vert_handle = glCreateShader(GL_VERTEX_SHADER);
  frag_handle = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(vert_handle, 1, &vertex_shader, 0);
  glShaderSource(frag_handle, 1, &fragment_shader, 0);
  glCompileShader(vert_handle);
  glCompileShader(frag_handle);
  glAttachShader(shader_handle, vert_handle);
  glAttachShader(shader_handle, frag_handle);
  glLinkProgram(shader_handle);

  texture_location = glGetUniformLocation(shader_handle, "Texture");
  proj_mtx_location = glGetUniformLocation(shader_handle, "ProjMtx");
  position_location = glGetAttribLocation(shader_handle, "Position");
  uv_location = glGetAttribLocation(shader_handle, "UV");
  colour_location = glGetAttribLocation(shader_handle, "Color");

  glGenBuffers(1, &vbo_handle);
  glBindBuffer(GL_ARRAY_BUFFER, vbo_handle);
  glBufferData(GL_ARRAY_BUFFER, vbo_max_size, NULL, GL_DYNAMIC_DRAW);

  glGenVertexArrays(1, &vao_handle);
  glBindVertexArray(vao_handle);
  glBindBuffer(GL_ARRAY_BUFFER, vbo_handle);
  glEnableVertexAttribArray(position_location);
  glEnableVertexAttribArray(uv_location);
  glEnableVertexAttribArray(colour_location);

  glVertexAttribPointer(position_location, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, pos));
  glVertexAttribPointer(uv_location, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, uv));
  glVertexAttribPointer(colour_location, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, col));
  glBindVertexArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  ImGuiIO& io = ImGui::GetIO();
  io.DeltaTime = 1.0f / 60.0f;                                  // Time elapsed since last frame, in seconds (in this sample app we'll override this every frame because our timestep is variable)
  io.KeyMap[ImGuiKey_Tab] = GLFW_KEY_TAB;                       // Keyboard mapping. ImGui will use those indices to peek into the io.KeyDown[] array.
  io.KeyMap[ImGuiKey_LeftArrow] = GLFW_KEY_LEFT;
  io.KeyMap[ImGuiKey_RightArrow] = GLFW_KEY_RIGHT;
  io.KeyMap[ImGuiKey_UpArrow] = GLFW_KEY_UP;
  io.KeyMap[ImGuiKey_DownArrow] = GLFW_KEY_DOWN;
  io.KeyMap[ImGuiKey_Home] = GLFW_KEY_HOME;
  io.KeyMap[ImGuiKey_End] = GLFW_KEY_END;
  io.KeyMap[ImGuiKey_Delete] = GLFW_KEY_DELETE;
  io.KeyMap[ImGuiKey_Backspace] = GLFW_KEY_BACKSPACE;
  io.KeyMap[ImGuiKey_Enter] = GLFW_KEY_ENTER;
  io.KeyMap[ImGuiKey_Escape] = GLFW_KEY_ESCAPE;
  io.KeyMap[ImGuiKey_A] = GLFW_KEY_A;
  io.KeyMap[ImGuiKey_C] = GLFW_KEY_C;
  io.KeyMap[ImGuiKey_V] = GLFW_KEY_V;
  io.KeyMap[ImGuiKey_X] = GLFW_KEY_X;
  io.KeyMap[ImGuiKey_Y] = GLFW_KEY_Y;
  io.KeyMap[ImGuiKey_Z] = GLFW_KEY_Z;

  io.RenderDrawListsFn = renderfn;
  //io.SetClipboardTextFn = ImImpl_SetClipboardTextFn;
  //io.GetClipboardTextFn = ImImpl_GetClipboardTextFn;

  loadFonts();

  ImGuiState& g = *GImGui;
  ImGuiStyle& style = g.Style;

  style.WindowPadding.x =  0;
  style.WindowPadding.y =  0;
  style.WindowRounding  =  0;
  style.ScrollbarWidth  = 12;

  for (int i = 0; i < ImGuiCol_COUNT; i++)
  {
    const char* name = ImGui::GetStyleColName(i);

    if (name == std::string("Text"))
      style.Colors[i] = whiteCol;

    else if (name == std::string("TitleBg"))
      style.Colors[i] = darkerGrey;
    else if (name == std::string("TitleBgCollapsed"))
      style.Colors[i] = darkerGrey;

    else if (name == std::string("Header"))
      style.Colors[i] = darkerGrey;
    else if (name == std::string("HeaderHovered"))
      style.Colors[i] = darkerGreyHovered;
    else if (name == std::string("HeaderActive"))
      style.Colors[i] = darkerGrey;
    else if (name == std::string("Border"))
      style.Colors[i] = blueHightlight;

    else if (name == std::string("CloseButton"))
      style.Colors[i] = ImVec4(0.0, 0.0, 0.0, 0.0);
    else if (name == std::string("CloseButtonHovered"))
      style.Colors[i] = ImVec4(0.0, 0.0, 0.0, 0.0);
    else if (name == std::string("CloseButtonActive"))
      style.Colors[i] = ImVec4(0.0, 0.0, 0.0, 0.0);

    else if (name == std::string("CheckMark"))
      style.Colors[i] = blueHightlight;

    else if (name == std::string("SliderGrab"))
      style.Colors[i] = blueHightlight;
    else if (name == std::string("SliderGrabActive"))
      style.Colors[i] = blueHightlight;

    else if (name == std::string("ScrollbarBg"))
      style.Colors[i] = darkerGrey;
    else if (name == std::string("ScrollbarGrab"))
      style.Colors[i] = midGrey;
    else if (name == std::string("ScrollbarGrabHovered"))
      style.Colors[i] = midGreyHovered;
    else if (name == std::string("ScrollbarGrabActive"))
      style.Colors[i] = blueHightlight;

    else if (name == std::string("Button"))
      style.Colors[i] = darkerGrey;
    else if (name == std::string("ButtonHovered"))
      style.Colors[i] = darkerGreyHovered;
  }


}

void GUI::loadFonts()
{
  ImGuiIO& io = ImGui::GetIO();
  //ImFont* my_font1 = io.Fonts->AddFontDefault();

  //FONT: http://www.1001fonts.com/aller-font.html
  ImFont* my_font2 = io.Fonts->AddFontFromFileTTF(FONTS_PATH "/Aller_Std_Rg.ttf", 16.0f);

  unsigned char* pixels;
  int width, height;
  io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);   // Load as RGBA 32-bits for OpenGL3 demo because it is more likely to be compatible with user's existing shader.

  GLuint tex_id;
  glGenTextures(1, &tex_id);
  glBindTexture(GL_TEXTURE_2D, tex_id);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

  // Store our identifier
  io.Fonts->TexID = (void *)(intptr_t)tex_id;
}

float width, height;
