#pragma once

#include "CVK_2/CVK_Defs.h"
#include "CVK_2/CVK_ShaderMinimal.h"
#include "CVK_2/CVK_FBO.h"

#include <array>

class Wave;

class ShaderOceanSimulationGerstner : public CVK::ShaderMinimal
{
public:
    ShaderOceanSimulationGerstner(GLuint shader_mask, const char** shaderPaths, CVK::CubeMapTexture *cubeMap);
    void update(float const & oceanWidth, float const & oceanDepth, float const & timeStep, std::vector<Wave*> const & waves, GLuint const & noiseMap, float const & noiseMapWidth, float const & noiseMapHeight, CVK::Texture & foamTexture, float const & foamIntensity, std::array<glm::vec4, 4> const & xzRangePoints, glm::vec3 const & camPosition, float const & shininess, glm::vec3 & lightPos, float const & maxAmplitude);

private:

    GLuint m_noiseMapSizeID;

    GLuint m_oceanWidthID;
    GLuint m_oceanDepthID;

    GLuint m_timeStepID;

    GLuint m_bottomLeftPointID;
    GLuint m_bottomRightPointID;
    GLuint m_topLeftPointID;
    GLuint m_topRightPointID;

    GLuint m_eyePosWsID;

	GLuint m_lightPosWsID;

	GLuint m_maxAmplitudeID;

    GLuint m_shineFactorID;
    GLuint m_foamIntensityID;

	GLuint m_noiseTextureID;
    GLuint m_foamTextureID;

    CVK::CubeMapTexture *m_cubeMapTexture;
    GLuint m_cubeMapTextureID;
};
