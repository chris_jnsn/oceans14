#pragma once
#include<string>
#include<vector>
#include<CVK_2/CVK_Texture.h>

class WaveSimulation;

class GUI
{
  public:
    GUI(unsigned int const & guiWidth, unsigned int const & guiHeight);
    ~GUI();

    void show();
    void hide();

    inline bool isVisible() { return m_visible; }
    inline unsigned int const & getWidth() { return m_width; }
    inline unsigned int const & getHeight() { return m_height; }

    inline void setWidth(unsigned int const & newWidth) { m_width = newWidth; }
    inline void setHeight(unsigned int const & newHeight) { m_height = newHeight; }

    void render(GLFWwindow* window);
    void setFPSVariable(int *finalFPS, double *renderTime);
    void setWireframeVariable(bool *wire);
    void setExternalCameraVariable(bool *externalCam);
    void setShowNormalsVariable(bool *norm);
    void setWaveSimulation(WaveSimulation * waveSim);
	void setShowReflectionsVariable(bool *reflect);
	void setShowSunVariable(bool *sun);
	void setShowFoamVariable(bool *foam);
	void setShininessVariable(float *shineFactor);
	void setFoamItensityVariable(float *foamItensity);
    void setOceanRoughnessVariable(float *roughness);


  private:
    void initGUI();
    void update(GLFWwindow* window);
    void loadFonts();
    void drawElements(GLFWwindow* window);
    void tab();
    std::string doubleDigitToString(double& number);


    bool m_visible;
    bool m_initialized;

    unsigned int m_width;
    unsigned int m_height;

    CVK::Texture *m_headerTexture;

	//Performance
    int *m_finalFPS;
    double *m_renderTime;

	//RENDERER INFO
	std::string m_renderTextA;
	std::string m_renderTextB;
	std::string m_renderTextC;

    //Simulation
    WaveSimulation * m_waveSimulation;

    //Visualization
    bool *m_wireframe;
    bool *m_useExternalCamera;
    bool *m_showNormals;
	bool *m_showReflections;
	bool *m_showSun;
	bool *m_showFoam;
	float *m_shininess;
	float *m_foamIntensitiy;
    float *m_oceanRoughness;
};
