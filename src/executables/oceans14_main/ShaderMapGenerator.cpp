#include "ShaderMapGenerator.h"


ShaderMapGenerator::ShaderMapGenerator(GLuint shader_mask, const char** shaderPaths) : CVK::ShaderMinimal(shader_mask,shaderPaths)
{
	m_noiseTextureID = glGetUniformLocation(m_ProgramID, "noiseMap");
}

void ShaderMapGenerator::update(CVK::FBO & noiseFBO)
{
	CVK::ShaderMinimal::update();

	glActiveTexture(GL_TEXTURE8);
  	glBindTexture(GL_TEXTURE_2D, noiseFBO.getColorTexture(0));
  	glUniform1i(m_noiseTextureID, 8);
}
