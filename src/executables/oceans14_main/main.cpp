//TODO:
    //different foam texture ?

//redefine paths for release build to take resources from binary location
#ifdef RELEASE_BUILD
    #pragma warning(disable : 4005) //disable warning for macro redefinition
    #define RESOURCES_PATH  "./"
    #define SHADERS_PATH    "./shaders"
    #define MODELS_PATH     "./models"
    #define TEXTURES_PATH   "./textures"
    #define FONTS_PATH      "./fonts"
    #pragma warning(enable : 4005) //enable warning for macro redefinition
#endif

#include <iostream>
#include <fstream>
#include <CVK_2/CVK_Framework.h>
#include "GUI.h"
#include "ShaderOceanRenderer.h"
#include "ShaderCubeMap.h"
#include "WaveSimulation.h"
#include "Noise.h"
#include "Wave.h"
#include "OceanCamera.h"
#include "GridProjector.h"

//define global variables:

//WINDOW
//unsigned int WINDOW_WIDTH = 1600;
//unsigned int WINDOW_HEIGHT = 1024;

unsigned int WINDOW_WIDTH = 800;
unsigned int WINDOW_HEIGHT = 600;

GLFWwindow *g_window;

//GUI
GUI *g_gui = nullptr;
unsigned int GUI_WIDTH = 250;
unsigned int GUI_HEIGHT = WINDOW_HEIGHT;
bool g_wireframeMode        = false;
bool g_showNormalsMode      = false;
bool g_fTenPressed          = false;
bool g_showReflectionsMode  = true;
bool g_showSunMode          = true;
bool g_showFoamMode         = false;
int g_fps;
float shininess = 100.0f;
float foamIntensity = 0.147f;
double g_renderTime;

CVK::Node * g_rangePoints;

//SCENE
WaveSimulation *g_waveSimulation;
Noise *g_noise;
CVK::Node *g_sceneNode;
CVK::Light *g_light;
CVK::Material *g_matCvlogo;
CVK::CubeMapTexture *g_cubeMapTexture;
CVK::Texture *g_foamTexture;
CVK::Node *g_cubeMapNode;
CVK::Node *g_sunNode;
std::vector<CVK::Node*> *g_intersectionPointNodes;

CVK::Texture *NoiseNormals;

CVK::Node *g_mainOceanCameraNode;
CVK::Node *g_frustumCornersNode;
CVK::Node *g_farPlaneNode;
bool g_showExternalCamera = false;
bool g_externalCameraSwitched = false;
float g_farPlaneRender = 60.0f;
float g_oceanRoughness = 0.2f;

glm::vec3 *g_lightDirection = new glm::vec3(1.0,0.5,-1.0);
glm::vec3 *g_lightPosition = new glm::vec3(-0.265, 0.35, -1.0);
glm::vec3 *g_cubeMapScaling = new glm::vec3(100,100,100);

//OCEAN
unsigned int const OCEAN_MAP_WIDTH  = 256;
unsigned int const OCEAN_MAP_HEIGHT = 256;

unsigned int const OCEAN_MESH_WIDTH = 256;
unsigned int const OCEAN_MESH_DEPTH = 256;


float g_oceanBaseHeight = 0.0f;
//CAMERA
CVK::Perspective g_projection(70.0f, WINDOW_WIDTH / (float) WINDOW_HEIGHT, 0.1f, 200.f);
OceanCamera * g_oceanCam = nullptr;
OceanCamera * g_oceanCamExternal = nullptr;

//adapt viewport to window size===============================================================
void resizeCallback(GLFWwindow *window, int w, int h) 
{
    g_oceanCam->setWidthHeight(w,h);
    g_oceanCam->getProjection()->updateRatio(w / (float) h);
    g_oceanCam->setCamPos(glm::vec3(0,3,2));
    g_oceanCamExternal->setWidthHeight(w, h);
    g_oceanCamExternal->getProjection()->updateRatio(w / (float)h);
    g_oceanCamExternal->setCamPos(glm::vec3(0,3,2));

    GUI_HEIGHT = h;
    g_gui->setHeight(GUI_HEIGHT);
    glViewport(0, 0, w, h);
}

void init_camera() 
{
    g_oceanCam = new OceanCamera(WINDOW_WIDTH, WINDOW_HEIGHT, &g_projection, g_gui);
    g_oceanCamExternal = new OceanCamera(WINDOW_WIDTH, WINDOW_HEIGHT, &g_projection, g_gui);

    glm::vec3 camCenter(
            0.0f,
            0.0f,
            0.0f
         );

    g_oceanCam->setCamPos(glm::vec3(0.0f, 0.0f, 0.0f));
    g_oceanCamExternal->setCamPos(glm::vec3(0.0f, 0.0f, 0.0f));

    CVK::State::getInstance()->setCamera(g_oceanCam);

}

void init_materials() 
{
    //g_matBlue = new CVK::Material(blue, blue, 10000000.0);
    g_matCvlogo = new CVK::Material(TEXTURES_PATH "/cv_logo.bmp", 1.0f, 0.75f, white, 100.0f);
}

void init_scene() 
{
    g_sceneNode = new CVK::Node("Scene");
    g_waveSimulation = new WaveSimulation(
        SIN_WAVES,
        OCEAN_MAP_WIDTH, OCEAN_MAP_HEIGHT,
        OCEAN_MESH_WIDTH,OCEAN_MESH_DEPTH,
        g_oceanBaseHeight, 
        g_oceanCam, g_cubeMapTexture);
	g_noise = new Noise(OCEAN_MAP_WIDTH, OCEAN_MAP_HEIGHT);

    //==================================================
    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(0.0f, 0.0f, 1.0f),        //DIRECTION
        0.13f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        16.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        4.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));

    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(1.0f, 0.0f, 1.0f),        //DIRECTION
        0.183f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        14.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        4.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));

    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(1.0f, 0.0f, 0.5f),        //DIRECTION
        0.073f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        24.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        4.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));


    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(0.5f, 0.0f, 1.0f),        //DIRECTION
        0.143f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        17.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        4.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));
    
    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(0.5f, 0.0f, 0.5f),        //DIRECTION
        0.073f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        12.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        4.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));

    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(1.0f, 0.0f, 0.0f),        //DIRECTION
        0.123f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        16.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        4.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));

    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(1.0f, 0.0f, 0.5f),        //DIRECTION
        0.223f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        21.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        4.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));

    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(0.0f, 0.0f, 0.5f),        //DIRECTION
        0.249f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        31.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        4.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));

    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(0.25f, 0.0f, 0.0f),        //DIRECTION
        0.241f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        28.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        4.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));

    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(0.25f, 0.0f, 0.25f),        //DIRECTION
        0.003f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        10.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        4.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));

    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(-1.0f, 0.0f, 1.0f),        //DIRECTION
        0.203f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        25.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        4.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));


    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(-1.0f, 0.0f, 0.25f),        //DIRECTION
        0.233f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        30.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        4.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));

    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(-0.25f, 0.0f, 0.35f),        //DIRECTION
        0.083f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        20.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        4.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));

    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(0.5f, 0.0f, 0.75f),        //DIRECTION
        0.183f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        24.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        5.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));

    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(1.0f, 0.0f, 0.45f),        //DIRECTION
        0.213f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        17.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        2.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));

    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(1.0f, 0.0f, -0.25f),        //DIRECTION
        0.133f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        17.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        7.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));

    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(0.1f, 0.0f, -0.17f),        //DIRECTION
        0.083f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        17.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        9.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));

    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(0.6f, 0.0f, -0.67f),        //DIRECTION
        0.033f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        10.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        3.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));

    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(0.3f, 0.0f, -0.37f),        //DIRECTION
        0.053f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        12.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        6.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));

    g_waveSimulation->addWaveToSimulation(new Wave(
        glm::vec3(0.79f, 0.0f, 0.6f),        //DIRECTION
        0.128f,                              //AMPLITUDE     - HEIGHT OF THE SINGLE WAVES
        18.0f,                               //WAVELENGTH    - WIDTH OF SINGLE WAVES
        8.0f                                //PHASE SHIFT   - MOVES THE WAVES
        ));

    std::cout << "Number of Waves: " << g_waveSimulation->getWaves().size() << std::endl;


    //==================================================
    /*
	for (int i = 0; i < 20; i++)
	{
        float newAmpl = 0.05f * (static_cast<float>(rand()) / static_cast<float>(RAND_MAX));

		g_waveSimulation->addWaveToSimulation(new Wave(
            glm::vec3(
                static_cast<float>(rand()) / static_cast<float>(RAND_MAX),
                0.0f, 
                static_cast<float>(rand()) / static_cast<float>(RAND_MAX)),
            newAmpl,
            (static_cast<float>(rand()) / static_cast<float>(RAND_MAX)) * 10,
            (static_cast<float>(rand()) / static_cast<float>(RAND_MAX) * 5
            )));
	}
    */
    

    g_oceanCam->setOceanBaseHeight(g_oceanBaseHeight);
    
    //set height restriction to very small value
    g_oceanCamExternal->setOceanBaseHeight(-100);

    g_gui->setWaveSimulation(g_waveSimulation);

    g_waveSimulation->setSimulationType(GERSTNER_WAVES);

    //create cube map
    const char * cubeMapPaths[] = 
    {
        
        /*textures_path "/skybox/right.png",
        textures_path "/skybox/left.png",
        textures_path "/skybox/up.png",
        textures_path "/skybox/down.png",
        textures_path "/skybox/front.png",
        textures_path "/skybox/back.png",*/
        
		TEXTURES_PATH "/SunnyDay/left.png",
		TEXTURES_PATH "/SunnyDay/right.png",
		TEXTURES_PATH "/SunnyDay/up.png",
		TEXTURES_PATH "/SunnyDay/down.png",
		TEXTURES_PATH "/SunnyDay/front.png",
		TEXTURES_PATH "/SunnyDay/back.png",
        
    };

    g_cubeMapTexture = new CVK::CubeMapTexture(cubeMapPaths);
	g_foamTexture = new CVK::Texture(TEXTURES_PATH "/sea_foam.png");

    CVK::Cube *cubeGeom = new CVK::Cube();

    g_cubeMapNode = new CVK::Node("cubeMapNode_up");
	g_cubeMapNode->setModelMatrix(glm::scale(*g_cubeMapNode->getModelMatrix(), *g_cubeMapScaling));
    g_cubeMapNode->setGeometry(cubeGeom);

    CVK::Material * whiteMat = new CVK::Material(white, white, 0.0);

	CVK::Sphere *sphere = new CVK::Sphere(3.0f);
	g_sunNode = new CVK::Node();
	g_sunNode->setGeometry(sphere);
	g_sunNode->setMaterial(whiteMat);

    g_mainOceanCameraNode = new CVK::Node();
    g_mainOceanCameraNode->setGeometry(new CVK::Sphere(0.03));
    g_mainOceanCameraNode->setMaterial(new CVK::Material(black, black, 0.0));

    g_frustumCornersNode = new CVK::Node();
    g_frustumCornersNode->setMaterial(whiteMat);

    g_farPlaneNode = new CVK::Node();
    g_farPlaneNode->setMaterial(whiteMat);

    g_intersectionPointNodes = new std::vector<CVK::Node*>();

    g_rangePoints = new CVK::Node();
    g_rangePoints->setMaterial(new CVK::Material(blue, blue, 0.0));


}

void init_gui()
{
    g_gui = new GUI(GUI_WIDTH,GUI_HEIGHT);
    g_gui->setFPSVariable(&g_fps, &g_renderTime);
    g_gui->setWireframeVariable(&g_wireframeMode);
    g_gui->setExternalCameraVariable(&g_showExternalCamera);
	g_gui->setShowNormalsVariable(&g_showNormalsMode);
	g_gui->setShowReflectionsVariable(&g_showReflectionsMode);
	g_gui->setShowSunVariable(&g_showSunMode);
	g_gui->setShowFoamVariable(&g_showFoamMode);
	g_gui->setShininessVariable(&shininess);
    g_gui->setOceanRoughnessVariable(&g_oceanRoughness);
	g_gui->setFoamItensityVariable(&foamIntensity);
    g_gui->hide();
}

CVK::Geometry * updateFarPlaneGeometry()
{
    CVK::Geometry * newFarPlaneGeom = new CVK::Geometry();

    std::vector<glm::vec4> * tmpVert = newFarPlaneGeom->getVertices();
    std::vector<glm::vec3> * tmpNorm = newFarPlaneGeom->getNormals();
    std::vector<unsigned int> * tmpIndex= newFarPlaneGeom->getIndex();
    std::vector<glm::vec2> * tmpUv = newFarPlaneGeom->getUVs();

    tmpVert->clear();
    tmpNorm->clear();
    tmpIndex->clear();
    tmpUv->clear();

    tmpVert->push_back(g_oceanCam->getFrustumCornersWS()[4]);
    tmpVert->push_back(g_oceanCam->getFrustumCornersWS()[5]);
    tmpVert->push_back(g_oceanCam->getFrustumCornersWS()[6]);
    tmpVert->push_back(g_oceanCam->getFrustumCornersWS()[7]);

    for(int i = 0 ; i < 4; i++)
    {
        tmpNorm->push_back(glm::vec3(0.0f,1.0f,0.0f));
        tmpUv->push_back(glm::vec2(0.0f,0.0f));
    }

    tmpIndex->push_back(0);
    tmpIndex->push_back(3);
    tmpIndex->push_back(2);
    tmpIndex->push_back(1);
    tmpIndex->push_back(2);
    tmpIndex->push_back(1);

    newFarPlaneGeom->createBuffers();

    return newFarPlaneGeom;
}

CVK::Geometry * updateRangePointsGeometry()
{
    CVK::Geometry * newRangePointsGeom = new CVK::Geometry();

    std::vector<glm::vec4> * tmpVert = newRangePointsGeom->getVertices();
    std::vector<glm::vec3> * tmpNorm = newRangePointsGeom->getNormals();
    std::vector<unsigned int> * tmpIndex= newRangePointsGeom->getIndex();
    std::vector<glm::vec2> * tmpUv = newRangePointsGeom->getUVs();

    tmpVert->clear();
    tmpNorm->clear();
    tmpIndex->clear();
    tmpUv->clear();

    tmpVert->push_back(glm::vec4(g_waveSimulation->getGridProjector().getXZRangePoints()[0]));
    tmpVert->push_back(glm::vec4(g_waveSimulation->getGridProjector().getXZRangePoints()[1]));
    tmpVert->push_back(glm::vec4(g_waveSimulation->getGridProjector().getXZRangePoints()[2]));
    tmpVert->push_back(glm::vec4(g_waveSimulation->getGridProjector().getXZRangePoints()[3]));

    for(int i = 0 ; i < 30; i++)
    {
        tmpNorm->push_back(glm::vec3(0.0f,1.0f,0.0f));
        tmpUv->push_back(glm::vec2(0.0f,0.0f));
    }

    tmpIndex->push_back(0);
    tmpIndex->push_back(2);
    tmpIndex->push_back(3);

    tmpIndex->push_back(3);
    tmpIndex->push_back(0);
    tmpIndex->push_back(1);

    newRangePointsGeom->createBuffers();

    return newRangePointsGeom;
}

CVK::Geometry * updateViewFrustumGeometry()
{
    CVK::Geometry * geom = new CVK::Geometry();

    std::vector<glm::vec4> * tmpGeometryVerticesCopy = geom->getVertices();
    tmpGeometryVerticesCopy->clear();

    for (int i = 0; i < g_oceanCam->getFrustumCornersWS().size(); i++)
    {
        tmpGeometryVerticesCopy->push_back(glm::vec4(
            g_oceanCam->getFrustumCornersWS().at(i).x, 
            g_oceanCam->getFrustumCornersWS().at(i).y, 
            g_oceanCam->getFrustumCornersWS().at(i).z,
            1));
    }


    std::vector<glm::vec3> * tmpNormals = geom->getNormals();
    std::vector<glm::vec2> * tmpUVs = geom->getUVs();

    tmpNormals->clear();
    tmpUVs->clear();

    for (int i = 0; i < 8; i++)
    {
        tmpNormals->push_back(glm::vec3(0, 1, 0));
        tmpUVs->push_back(glm::vec2(0, 0));
    }

    std::vector<GLuint> * tmpIndex = geom->getIndex();
    tmpIndex->clear();

    //NEAR
    tmpIndex->push_back(0);
    tmpIndex->push_back(3);
    tmpIndex->push_back(2);
    tmpIndex->push_back(1);

    tmpIndex->push_back(4);
    tmpIndex->push_back(0);
    tmpIndex->push_back(1);
    tmpIndex->push_back(5);

    tmpIndex->push_back(2);
    tmpIndex->push_back(3);
    tmpIndex->push_back(7);
    tmpIndex->push_back(6);

    geom->createBuffers();

    return geom;
}

void updateIntersectionPointNodes()
{
    for(int i = 0; i < g_intersectionPointNodes->size(); i++)
    {
        delete g_intersectionPointNodes->at(i);
    }

    g_intersectionPointNodes->clear();

    for(int i = 0; i <  g_waveSimulation->getGridProjector().getFrustumOceanIntersectionsWS().size(); i++)
    {
        CVK::Node * newIntersectionNode = new CVK::Node();
        newIntersectionNode->setGeometry(new CVK::Sphere(0.005));

        glm::vec3 color;

        if(g_waveSimulation->getGridProjector().getFrustumOceanIntersectionsWS()[i].y == g_oceanBaseHeight)
            color = RedCol;
        else if(g_waveSimulation->getGridProjector().getFrustumOceanIntersectionsWS()[i].y > g_oceanBaseHeight)
            color = CadetBlue;
        else 
            color = GreenCol;

        newIntersectionNode->setMaterial(new CVK::Material(color, color, 0.0));

        newIntersectionNode->setModelMatrix(
            glm::translate(*newIntersectionNode->getModelMatrix(),g_waveSimulation->getGridProjector().getFrustumOceanIntersectionsWS()[i])
            );


        g_intersectionPointNodes->push_back(newIntersectionNode);

    }

    //add projector point
    CVK::Node * projectorPoint = new CVK::Node();
    projectorPoint->setGeometry(new CVK::Sphere(0.005));

    glm::vec3 color = Yellow;

    projectorPoint->setMaterial(new CVK::Material(color, color, 0.0));

    projectorPoint->setModelMatrix(
        glm::translate(*projectorPoint->getModelMatrix(), g_waveSimulation->getGridProjector().getProjectorPoint())
        );

    g_intersectionPointNodes->push_back(projectorPoint);
}

int main() 
{

    // Init GLFW and GLEW
    glfwInit();
    CVK::useOpenGL33CoreProfile();
    g_window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "oceans14", 0, 0);
    glfwSetWindowPos(g_window, 100, 50);
    glfwMakeContextCurrent(g_window);
    glfwSetWindowSizeCallback(g_window, resizeCallback);
    glewInit();
	init_gui();

    CVK::State::getInstance()->setBackgroundColor(Black);
    glm::vec3 BgCol = CVK::State::getInstance()->getBackgroundColor();
    glClearColor(BgCol.r, BgCol.g, BgCol.b, 0.0);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    init_camera();
    init_materials();
    init_scene();

    //init shader for ocean 
    const char *sceneRendererNames[3] = { SHADERS_PATH "/OceanRendering/OceanRenderer.vert", SHADERS_PATH
     "/OceanRendering/OceanRenderer.geom", SHADERS_PATH "/OceanRendering/OceanRenderer.frag" };
	ShaderOceanRenderer oceanShader(VERTEX_SHADER_BIT | GEOMETRY_SHADER_BIT | FRAGMENT_SHADER_BIT, sceneRendererNames, g_cubeMapTexture, g_foamTexture);
    CVK::State::getInstance()->setShader(&oceanShader);

    //init shader for cubemap 
    const char *cubeMapShaderNames[2] = { SHADERS_PATH "/OceanRendering/CubeMap.vert", SHADERS_PATH "/OceanRendering/CubeMap.frag" };
    ShaderCubeMap cubeMapShader(VERTEX_SHADER_BIT | FRAGMENT_SHADER_BIT, cubeMapShaderNames, g_cubeMapTexture);
    CVK::State::getInstance()->setShader(&cubeMapShader);

    //init shader for scene
    const char *phongShaderNames[2] = { SHADERS_PATH "/naive/RenderShader.vert", SHADERS_PATH "/naive/RenderShader.frag" };
    CVK::ShaderPhong phongShader(VERTEX_SHADER_BIT | FRAGMENT_SHADER_BIT, phongShaderNames);
    CVK::State::getInstance()->setShader(&phongShader);

    //define Light Sources
    g_light = new CVK::Light(glm::vec4(-1, 4, 1, 1), white, glm::vec3(0, 0, 0), 1.0f, 0.0f);
    CVK::State::getInstance()->addLight(g_light);
    CVK::State::getInstance()->updateSceneSettings(white, 0, white, 1, 10, 1);

	//NoiseNormals = new CVK::Texture(TEXTURES_PATH "/normal6.png");

    float currentTime = static_cast<float>(glfwGetTime());
    float simulationTimeStep = 0;

    bool externalModeStarted = false;
    while (!glfwWindowShouldClose(g_window)) {
		
		g_sunNode->setModelMatrix(glm::translate(glm::mat4(1.0), glm::vec3(*g_lightPosition * (*g_cubeMapScaling * 0.9f))));
        g_cubeMapNode->setModelMatrix(glm::translate(glm::mat4(1.0), g_oceanCam->getCamPos()));
        g_cubeMapNode->setModelMatrix(glm::scale(*g_cubeMapNode->getModelMatrix(), *g_cubeMapScaling));
        

        if (!g_externalCameraSwitched && g_showExternalCamera)
        {
            OceanCamera * oldCam = g_oceanCamExternal;
            g_oceanCamExternal = new OceanCamera(*g_oceanCam);
            delete oldCam;
            g_oceanCamExternal->setCamPos(g_oceanCam->getCamPos() - glm::vec3(0.2) * g_oceanCam->getCamDirection());

            CVK::Geometry * oldGeom = g_frustumCornersNode->getGeometry();
            g_frustumCornersNode->setGeometry(updateViewFrustumGeometry());
            delete oldGeom;

            oldGeom = g_farPlaneNode->getGeometry();
            g_farPlaneNode->setGeometry(updateFarPlaneGeometry());
            delete oldGeom;

            updateIntersectionPointNodes();

            g_externalCameraSwitched = true;
        }

        if (g_externalCameraSwitched && !g_showExternalCamera)
            g_externalCameraSwitched = false;

        //DEBUG:
        //CVK::Geometry * oldGeom = g_rangePoints->getGeometry();
        //g_rangePoints->setGeometry(updateRangePointsGeometry());
        //delete oldGeom;

        //delta time
        float newTime = static_cast<float>(glfwGetTime());
        float deltaT = newTime - currentTime;
        currentTime = newTime;

        simulationTimeStep += deltaT;

        g_fps = static_cast<int>(1.0f / deltaT);

        
        if (glfwGetKey(g_window, GLFW_KEY_ESCAPE))
            break; //quit

        if (glfwGetKey(g_window, GLFW_KEY_F10))
            g_fTenPressed = true;

        if (g_fTenPressed && !glfwGetKey(g_window, GLFW_KEY_F10)) {
            if (g_gui->isVisible())
                g_gui->hide();
            else
                g_gui->show();

            g_fTenPressed = false;
        }

        int currWidth, currHeight;

        glfwGetWindowSize(g_window,&currWidth,&currHeight);

        if(currWidth != WINDOW_WIDTH)
            WINDOW_WIDTH = currWidth;

        if(currHeight != WINDOW_HEIGHT)
            WINDOW_HEIGHT = currHeight;

		//UPDATE NOISE=======================================
		g_noise->update(simulationTimeStep);
		//===================================================

        //UPDATE OCEAN=======================================
        g_oceanCam->calculateFrustumCornersWS(g_farPlaneRender + g_oceanCam->getCamPos().y * 2);
        bool renderOcean = g_waveSimulation->updateSimulation();
        //===================================================


        glViewport(0,0,WINDOW_WIDTH,WINDOW_HEIGHT);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_BLEND);
        glEnable(GL_DEPTH_TEST);

        //Update sphere of main camera and frustum, if external camera should be used
        if (g_showExternalCamera && renderOcean)
        {
            g_mainOceanCameraNode->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(g_oceanCam->getCamPos())));
            CVK::State::getInstance()->setCamera(g_oceanCamExternal);
            
            //give the external camera a very low ampl value, so external camera can move under the ocean line
            g_oceanCamExternal->update(g_window, -100);
        }

        else
        {
            CVK::State::getInstance()->setCamera(g_oceanCam);
            g_oceanCam->update(g_window, g_waveSimulation->getMaxAmplitude());
        }
        
        //RENDER SKYBOX =====================================
        CVK::State::getInstance()->setShader(&cubeMapShader);
        cubeMapShader.update();
        g_cubeMapNode->render();
        //===================================================
        
        //RENDER OCEAN ======================================

        if(renderOcean)
        {
            CVK::State::getInstance()->setLight(0, g_light);
            g_waveSimulation->render(g_wireframeMode,simulationTimeStep, g_noise->getNoiseMapFBO().getColorTexture(0), *g_foamTexture, foamIntensity, *g_oceanCam, shininess, *g_lightPosition, g_waveSimulation->getMaxAmplitude());
            /*
            CVK::State::getInstance()->setShader(&oceanShader);
            oceanShader.update(
                g_waveSimulation->getHeightMapFBO(),
                g_waveSimulation->getGridProjector().getXZRangePoints(),
                &g_oceanCam->getCamPos(), 
                g_showNormalsMode, g_showReflectionsMode, g_showSunMode, g_showFoamMode,
                shininess, foamIntensity);

            CVK::State::getInstance()->setLight(0, g_light);

            if(!g_wireframeMode)
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            else
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            
            g_waveSimulation->getWaveSimulationNode().render();
            */
        }
        
        //===================================================

        //RENDER SCENE=======================================
        CVK::State::getInstance()->setShader(&phongShader);
        phongShader.update();
        CVK::State::getInstance()->setLight(0, g_light);


        g_sceneNode->render();
        //g_sunNode->render();
        //DEBUG:
        //if(renderOcean)
        //    g_rangePoints->render();

        //if external camera mode is on, render sphere for normal ocean camera
        if (g_showExternalCamera && renderOcean)
        {
            //RENDER FRUSTUM IN WIRESPACE
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            g_frustumCornersNode->setModelMatrix(glm::mat4(1.0));
            g_farPlaneNode->setModelMatrix(glm::mat4(1.0));
            g_frustumCornersNode->render(GL_QUADS);
            g_farPlaneNode->render(GL_QUADS);

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            g_mainOceanCameraNode->render();

            for(std::vector<CVK::Node*>::iterator it = g_intersectionPointNodes->begin(); it != g_intersectionPointNodes->end(); ++it)
                (*it)->render();

        }
       
        //===================================================

        //RENDER GUI ========================================
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        g_gui->render(g_window);
        //===================================================
        
        
        g_renderTime = glfwGetTime() - currentTime;
        glfwSwapBuffers(g_window);
        glfwPollEvents();
    }

    glfwDestroyWindow(g_window);
    glfwTerminate();
    return 0;
}

