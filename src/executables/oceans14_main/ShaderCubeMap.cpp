#include "ShaderCubeMap.h"


ShaderCubeMap::ShaderCubeMap(GLuint shader_mask, const char** shaderPaths, CVK::CubeMapTexture *cubeMap) : CVK::ShaderMinimal(shader_mask,shaderPaths)
{
	//Textures
	m_cubeMapTexture = cubeMap;
	m_cubeMapTextureID = glGetUniformLocation( m_ProgramID, "cubeTexture");
}

void ShaderCubeMap::update()
{
	if (m_cubeMapTexture)
	{	
		glUniform1i( m_cubeMapTextureID, 0);
		glActiveTexture(CUBE_MAP_TEXTURE_UNIT);
		m_cubeMapTexture->bind();
	}

	CVK::ShaderMinimal::update();
}