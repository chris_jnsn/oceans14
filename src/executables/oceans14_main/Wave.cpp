#include "Wave.h"

const float GRAVITY     = 9.81f;
const float DOUBLE_PI   = 2.0f * glm::pi<float>();

Wave::Wave()
{

}

Wave::Wave(glm::vec3 const & direction, float const & amplitude, float const & waveLength, float const & phaseShift)
{
    m_direction     = direction;
    m_amplitude     = amplitude;
    m_waveLength    = waveLength;
    
    m_parameterK    = DOUBLE_PI / m_waveLength;
    m_phaseVelocity = glm::sqrt((GRAVITY * waveLength) / DOUBLE_PI);
    m_frequency     = m_phaseVelocity / waveLength;

    m_phaseShift    = phaseShift;
}

Wave::~Wave()
{

}

void Wave::setWaveLength(float const & waveLength)
{
    m_waveLength = waveLength;

    m_parameterK = DOUBLE_PI / m_waveLength;
    m_phaseVelocity = glm::sqrt((GRAVITY * waveLength) / DOUBLE_PI);
    m_frequency = m_phaseVelocity / waveLength;
}