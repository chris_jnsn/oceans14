#include "OceanCamera.h"

OceanCamera::OceanCamera( int width, int height, CVK::Projection *projection, GUI * gui)
    : CVK::Pilotview(width,height,projection), m_gui(gui)
{
    m_speed = 0.15f;
    m_frustumCornersWS = std::array < glm::vec4, 8 >();
}


OceanCamera::~OceanCamera()
{

}

void OceanCamera::update(GLFWwindow* window, float const & maxAmpl)
{
	double x, y;

	glfwGetCursorPos( window, &x, &y);  
	if (glfwGetMouseButton( window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS
        && (!m_gui->isVisible() || x < m_width - m_gui->getWidth()))
	{	
		float changeX = ((float) x - m_oldX) * m_sensitivity;
		float changeY = - ((float) y - m_oldY) * m_sensitivity;

        //speed reduction to prevent ocean borders from beeing visible
        changeX *= 0.2;
        changeY *= 0.2;

		m_theta -= changeY;
		if (m_theta < 0.01f) m_theta = 0.01f;
		else if (m_theta > glm::pi<float>() - 0.01f) m_theta = glm::pi<float>() - 0.01f;

		m_phi -= changeX;	
		if (m_phi < 0) m_phi += 2*glm::pi<float>();
		else if (m_phi > 2*glm::pi<float>()) m_phi -= 2*glm::pi<float>();

		m_oldX = (float) x;
		m_oldY = (float) y;

		m_direction.x = sin(m_theta) * sin(m_phi);
		m_direction.y = cos(m_theta);
		m_direction.z = sin(m_theta) * cos(m_phi);

	}
	else
	{
		m_oldX = (float) x;
		m_oldY = (float) y;
	}

    float currSpeed;
    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
        currSpeed = m_speed / 3.0f;
    else
        currSpeed = m_speed;

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        m_cameraPos += currSpeed * m_direction;

    if(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        m_cameraPos += currSpeed * -m_direction;

    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        m_cameraPos += currSpeed * -glm::cross(m_direction,m_up);

    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        m_cameraPos += currSpeed * glm::cross(m_direction, m_up);

    if(m_cameraPos.y < m_oceanBaseHeight + maxAmpl*10 + 6.0)
        m_cameraPos.y = m_oceanBaseHeight + maxAmpl*10 + 6.0;

    m_viewmatrix = glm::lookAt( m_cameraPos, m_cameraPos + m_direction, m_up);  
    
}

void OceanCamera::calculateFrustumCornersWS(float const & farPlaneToUse)
{
	float finalFarPlane = farPlaneToUse;

	if(finalFarPlane < 0 || finalFarPlane > m_projection->getFar())
		finalFarPlane = m_projection->getFar();

    //create new projection matrix with less far value
	glm::mat4 frustumCornersProjection = glm::perspective( 
		static_cast<CVK::Perspective*>(m_projection)->getFov(),
		static_cast<CVK::Perspective*>(m_projection)->getRatio(),
		m_projection->getNear(),
		finalFarPlane);
    
    //inverse viewprojection
    glm::mat4 invVP = glm::inverse(frustumCornersProjection * m_viewmatrix);

    //use inverse viewprojection to get from NDC back to WS
    //ORDER: CW starting top left, first for near plane, then far plane
    m_frustumCornersWS =
    {
        //NEAR
        glm::vec4(invVP * glm::vec4(-1.0f, 1.0f,-1.0f, 1.0f)),      //ntl
        glm::vec4(invVP * glm::vec4(1.0f, 1.0f,-1.0f, 1.0f)),       //ntr
        glm::vec4(invVP * glm::vec4(1.0f,-1.0f,-1.0f, 1.0f)),       //nbr
        glm::vec4(invVP * glm::vec4(-1.0f,-1.0f,-1.0f, 1.0f)),      //nbl

        //FAR
        glm::vec4(invVP * glm::vec4(-1.0f, 1.0f, 1.0f, 1.0f)),      //ftl
        glm::vec4(invVP * glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)),       //ftr
        glm::vec4(invVP * glm::vec4(1.0f, -1.0f, 1.0f, 1.0f)),      //fbr
        glm::vec4(invVP * glm::vec4(-1.0f, -1.0f, 1.0f, 1.0f)),     //fbl

    };

    //persp. division
    for (int i = 0; i < 8; i++)
    {
        m_frustumCornersWS[i] /= m_frustumCornersWS[i].w;
    }
    
}