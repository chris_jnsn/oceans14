#pragma once

#include <vector>
#include <CVK_2/CVK_Framework.h>

enum SIMULATION_TYPE
{
    SIN_WAVES,
    GERSTNER_WAVES,
    NUMBER_OF_SIMULATION_TYPES
};

class Wave;
class ShaderOceanSimulationSinus;
class ShaderOceanSimulationGerstner;
class ShaderSimpleTexture;
class GridProjector;
class OceanCamera;

class WaveSimulation
{
public:
    WaveSimulation(
        SIMULATION_TYPE simulationType,
        unsigned int const & oceanMapWidth, unsigned int const & oceanMapHeight,
        unsigned int const & oceanMeshWidth, unsigned int const & oceanMeshDepth, 
        float const & oceanBaseHeight,
        OceanCamera * oceanCam, CVK::CubeMapTexture * cubeMap);
    ~WaveSimulation();

    void addWaveToSimulation(Wave * newWave);

    bool updateSimulation();
    void render(bool const & wireFrame, float const & timeStep, GLuint const & noiseMap, CVK::Texture & foamTexture, float const & foamIntensity, OceanCamera & oceanCam, float const & shininess, glm::vec3 & lightPos, float const & maxAmplitude);

    inline CVK::Node & getWaveSimulationNode() {return *m_waveSimulationNode;}

    inline SIMULATION_TYPE getWaveSimulationType() { return m_simulationType; }

    inline std::vector<Wave*> const & getWaves() { return *m_waves; }

    inline float const & getMaxAmplitude() {return m_maxAmplitude;}

    inline void setSimulationType(SIMULATION_TYPE type)
    {
    	if(type >= NUMBER_OF_SIMULATION_TYPES)
    		std::cout << "Error in WaveSimulation::setSimulationType - wave simulation type with enum index of " << type << " not implemented!" << std::endl;
    	else
    		m_simulationType = type;
    }

    inline CVK::FBO & getHeightMapFBO() {return *m_heightmapFBO;}

    GridProjector & getGridProjector();

protected:

	void initWaveGeometry();
	void initShader(CVK::CubeMapTexture * cubeMap);

private:

    unsigned int m_oceanMapWidth;
    unsigned int m_oceanMapHeight;

    unsigned int m_oceanMeshWidth;
    unsigned int m_oceanMeshDepth;

    float m_oceanBaseHeight;
    float m_maxAmplitude;

    SIMULATION_TYPE m_simulationType;
    std::vector<Wave*> * m_waves;

    CVK::Node *m_waveSimulationNode;
    CVK::Geometry *m_oceanGeometry;

    ShaderOceanSimulationSinus *m_oceanSimulationSinusShader;
    ShaderOceanSimulationGerstner *m_oceanSimulationGerstnerShader;
    ShaderSimpleTexture *m_shaderSimpleTexture;

    GridProjector *m_gridProjector;

    CVK::FBO *m_heightmapFBO;

    CVK::Ortho *m_sfqOrtho;
    CVK::Trackball *m_sfqCamera;
    CVK::Node *m_sfqNode;
    CVK::Plane *m_sfqPlane;
};