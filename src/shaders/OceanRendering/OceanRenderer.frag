#version 330 core

const float AirIOR = 1.0;
const float WaterIOR = 1.33;
float R_0 = (AirIOR - WaterIOR) / (AirIOR + WaterIOR);

in vec3 passPosition;
in vec3 passNormal;
in vec2 passTcoord;
in vec3 normalWorld;
in vec3 passPerlinNoiseWorld;
in vec3 passPerlinNoise;

in float height;

in float fogExponent;

in vec3 incident;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

uniform struct
{
  float kd, ks, kt;
    vec3 diffColor;
  vec3 specColor;
    float shininess;
} mat;

uniform struct LIGHT
{
  vec4 pos;    //pos.w = 0 dir. light, pos.w = 1 point light
  vec3 col;
  vec3 spot_direction;
  float spot_exponent;
  float spot_cutoff;  // no spotlight if cutoff = 0
} light[8]; //MAX_LIGHTS

uniform int numLights;
uniform vec3 lightAmbient;

uniform int useColorTexture;
uniform sampler2D colorTexture;
uniform sampler2D foamTexture;

uniform samplerCube ReflectionMap;

uniform float showNormals;
uniform float showReflections;
uniform float showSun;
uniform float showFoam;
uniform float shineFactor;
uniform float foamFactor;
uniform float foamIntensity;

out vec4 fragmentColor;

float fresnelValue(vec3 ViewVector)
{
R_0 *= R_0;

float cosTheta = dot(ViewVector, normalize(passPerlinNoiseWorld));
float R_theta = R_0 + (1.0 - R_0) * pow(1.0 - cosTheta, 5.0);
return R_theta;
}

void main()
{
	fragmentColor.rgb = texture( colorTexture, passTcoord ).rgb;
	fragmentColor.a = 1.0;

  vec4 colorFresnel = fragmentColor;

  float fresnelPower = normalize(fresnelValue(incident));

  if(showReflections == 1.0)
  {
      //Reflection
      vec4 reflectionValue = normalize(texture(ReflectionMap, reflect(incident, normalize(passPerlinNoiseWorld))));
      //vec4 refractionValue = vec4(0.32, 0.39, 0.56, 1.0);
      vec4 refractionValue = normalize(texture(ReflectionMap, refract(incident, normalize(passPerlinNoiseWorld), 0.65)));

      float reflectionFactor = pow(1.0 + dot(incident, normalize(passPerlinNoiseWorld)), fresnelPower);
      colorFresnel = mix( reflectionValue, refractionValue, reflectionFactor );

      if(showFoam != 1.0)
      {
      fragmentColor += colorFresnel;
      }
    }

  if(showFoam == 1.0)
  {
    /* Old implementation
    //Foam
    float foamValue = dot( vec3(0,1,0), normalize(normalWorld));

    if (foamValue < foamFactor)
    {
    vec4 foamColor = texture(foamTexture, passTcoord);
    vec4 colorFresnelFoam = mix(foamColor, colorFresnel, foamValue * foamIntensity);
    //vec4 colorFresnelFoam = mix(vec4(1, 1, 1, 1), fragmentColor, foamValue );

    fragmentColor += colorFresnelFoam;
    }
    */

      vec4 foamColor = texture(foamTexture, passTcoord);

      vec4 colorFoam = mix(colorFresnel, foamColor, height * (foamIntensity));

      fragmentColor += colorFoam;

  }


  if(showSun == 1.0)
  {
    /* Old implementation
    //Light
    for ( int i = 0; i < numLights; i++)
    {

    vec3 L = normalize( passPosition.xyz - vec3(light[i].pos)  );
    vec3 L = normalize(vec3(1,0.5,-0.5));

    vec3 R_light = 2 * dot(normalize(passNormal), L) * normalize(passNormal) - L;

    float specularLight = pow( max( dot(-incident, R_light), 0), shineFactor);
    vec3 specular = mat.ks * light[i].col * specularLight;

    fragmentColor.rgb += specular;
    }
    */


    for ( int i = 0; i < numLights; i++)
    {
		vec3 L = normalize(-vec3(1,0.5,-0.5));

		vec3 E = normalize(incident);
		vec3 R = reflect(-L, passPerlinNoiseWorld);

		float specular = pow( max(dot(R, E), 0.0), shineFactor );
		vec3 specularLight = mat.ks * light[i].col * specular;

		fragmentColor.rgb += specularLight;
    }
  }

	//DEBUG: NORMALS
	if(showNormals == 1.0)
		fragmentColor.rgb = passPerlinNoiseWorld;
}
