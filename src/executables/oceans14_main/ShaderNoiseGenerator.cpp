#include "ShaderNoiseGenerator.h"
#include "Wave.h"

ShaderNoiseGenerator::ShaderNoiseGenerator(GLuint shader_mask, const char** shaderPaths) : CVK::ShaderMinimal(shader_mask,shaderPaths)
{
	m_timeStepID = glGetUniformLocation(m_ProgramID, "time");
}

void ShaderNoiseGenerator::update(float const & timeStep)
{
	CVK::ShaderMinimal::update();

    glUniform1f(m_timeStepID, timeStep);
}