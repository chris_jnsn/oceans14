#pragma once

#include "CVK_2/CVK_Defs.h"
#include "CVK_2/CVK_Camera.h"
#include "CVK_2/CVK_Light.h"
#include "CVK_2/CVK_Material.h"
#include "CVK_2/CVK_Geometry.h"
#include "CVK_2/CVK_ShaderMinimal.h"
#include "CVK_2/CVK_FBO.h"

#include <array>

class ShaderOceanRenderer : public CVK::ShaderMinimal
{
public:
    ShaderOceanRenderer( GLuint shader_mask, const char** shaderPaths, CVK::CubeMapTexture *cubeMap, CVK::Texture *texture);
	void update( CVK::FBO & heightMapFBO, std::array<glm::vec4, 4> const & xzRangePoints, glm::vec3 *camPosition, bool showNormals, bool showReflections, bool showSun, bool showFoam, float shininess, float foamIntensity);
	void update( CVK::Node* node);

private:
	GLuint m_kdID, m_ksID, m_ktID;
	GLuint m_diffuseID, m_specularID, m_shininessID;
	GLuint m_lightambID;

	GLuint m_numLightsID;
	GLuint m_lightposID[MAX_LIGHTS], m_lightcolID[MAX_LIGHTS], m_lightsdirID[MAX_LIGHTS], m_lightsexpID[MAX_LIGHTS], m_lightscutID[MAX_LIGHTS];

	GLuint m_useColorTexture, m_colorTextureID;

    GLuint m_heightMapID;
    GLuint m_normalMapID;
	GLuint m_noiseMapID;

    GLuint m_bottomLeftPointID;
    GLuint m_bottomRightPointID;
    GLuint m_topLeftPointID;
    GLuint m_topRightPointID;

    GLuint m_showNormalsID;
	GLuint m_showReflectionsID;
	GLuint m_showSunID;
	GLuint m_showFoamID;
	GLuint m_shineFactorID;
	GLuint m_foamIntensityID;

	GLuint m_fogcolID, m_fogstartID, m_fogendID, m_fogdensID, m_fogmodeID;

	GLuint m_camPos;

	CVK::CubeMapTexture *m_cubeMapTexture;
	GLuint m_cubeMapTextureID;

	CVK::Texture *m_foamTexture;
	GLuint m_foamTextureID;
};
