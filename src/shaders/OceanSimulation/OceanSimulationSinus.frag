#version 330 core

//IMPORTANT: keep this const value in sync with the corresponding const value in the WaveSimulation class!
const int MAX_NUMBER_OF_WAVES = 60;

struct Wave
{
	vec3  direction;
    float amplitude;
    float waveLength;
    float parameterK;
    float frequency;
    float phaseVelocity;
    float phaseShift;
};

uniform int oceanWidth;
uniform int oceanHeight;
uniform float timeStep;

uniform Wave waves[MAX_NUMBER_OF_WAVES];

in vec2 passUV;

layout(location = 0) out vec4 posNext; 
layout(location = 1) out vec4 normNext; 

void main() 
{ 
	vec3 newPosition = vec3(passUV.x, 0.0,passUV.y);

	float height = 0;

	for(int i = 0; i < MAX_NUMBER_OF_WAVES; i++)
	{
		height += waves[i].amplitude * sin(
			waves[i].parameterK * dot(waves[i].direction, newPosition) -
			waves[i].frequency * timeStep * waves[i].phaseVelocity +
			waves[i].phaseShift
			);
	}

	//calculate new height:
	newPosition.y = height;
  
	posNext = vec4(0.0,height,0.0,1.0);

	//gets calculated in OceanRenderer.geom in case of sinus waves
	normNext = vec4(0.0);
}