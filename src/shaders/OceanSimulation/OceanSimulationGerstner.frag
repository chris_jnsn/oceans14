#version 330 core

const float AirIOR = 1.0;
const float WaterIOR = 1.33;
float R_0 = (AirIOR - WaterIOR) / (AirIOR + WaterIOR);

in vec4 passPos;
in vec4 passPosWS;
in vec3 passNormal;
in vec3 passTangent;
in vec3 passBitangent;
in mat3 passNormalMatrix;

in vec2 passTC;
in vec3 passSightVec;

uniform vec3 lightPos;
uniform vec2 noiseMapSize;
uniform sampler2D noiseMap;
uniform sampler2D foamTexture;
uniform samplerCube cubeMap;
uniform float shineFactor;
uniform float foamIntensity;
uniform float maxAmp;

out vec4 fragmentColor;

vec3 calculateNormal(vec2 texCoord)
{
	int topOffset = -1;
	int rightOffset = 1;
	float texelSizeX = 1.0f / noiseMapSize.x ;
	float texelSizeY = 1.0f / noiseMapSize.y ;

	if(texCoord.x > 1.0 - texelSizeX)
		rightOffset = -1;

	if(texCoord.y < texelSizeY)
		topOffset = 1;

	float  heightCurr = texture (noiseMap, texCoord).x;
	float  heightTop = texture (noiseMap, texCoord + texCoord * vec2( 0, topOffset)).x;
	float  heightRight = texture (noiseMap, texCoord + texCoord * vec2( rightOffset,  0)).x;

	//calc normal and bring to range 0..1
	vec3 norm = vec3
	(
	 ((heightCurr - heightTop) + 1) / 2.0,
	 ((heightCurr - heightRight) + 1) / 2.0,
	 1.0
	);

	return normalize(norm);
}


vec3 perlinNormalMapping(vec3 normal, vec3 tangent, vec3 bitangent, vec2 texCoord)
{
	vec3 noiseNormal = calculateNormal(texCoord);

	noiseNormal = (noiseNormal - 0.5) * 2;
	noiseNormal = normalize(noiseNormal);
	mat3 world2texRotation = mat3(tangent, bitangent, normal);
	mat3 tex2worldRotation = transpose(world2texRotation);
	vec3 noiseNormalWorld = tex2worldRotation * noiseNormal;
	return normalize(noiseNormalWorld);
}

float fresnelValue(vec3 ViewVector, vec3 normal)
{
	R_0 *= R_0;

	float cosTheta = dot(ViewVector, normalize(normal));
	float R_theta = R_0 + (1.0 - R_0) * pow(1.0 - cosTheta, 5.0);
	return R_theta;
}

float scale(float max, float current)
{
	float result = ((current * 100.0) / max) / 100.0;
	return result;
}

void main()
{
	vec4 oceanColor =  vec4(1.0, 1.0, 1.0, 0.0);

	const float scaleFactorFoam = 0.0025;
	const float scaleFactorNoise = 0.025;
	//vec2 finalTC = passTC;
	vec2 finalTC_noise = vec2(mod(abs(passPosWS.x) * scaleFactorNoise ,1.0),mod(abs(passPosWS.z) * scaleFactorNoise,1.0));
	vec2 finalTC_foam = vec2(mod(abs(passPosWS.x) * scaleFactorFoam ,1.0),mod(abs(passPosWS.z) * scaleFactorFoam,1.0));

	fragmentColor = oceanColor;

	vec3 normalWS = perlinNormalMapping(passNormal, passTangent, passBitangent, finalTC_noise);
	vec3 normalCS = passNormalMatrix * normalWS;

	//REFLECTIONS:
		vec4 colorFresnel = fragmentColor;
		float fresnelPower = normalize(fresnelValue(passSightVec, normalWS));

		//Reflection
		vec4 reflectionValue = normalize(texture(cubeMap, reflect(passSightVec, normalize(normalWS))));
		vec4 refractionValue = vec4(0.12, 0.19, 0.56, 1.0);
		//vec4 refractionValue = normalize(texture(cubeMap, refract(passSightVec, normalize(normalWS), 0.65)));

		float reflectionFactor = pow(1.0 + dot(passSightVec, normalize(normalWS)), fresnelPower);
		colorFresnel = mix( reflectionValue, refractionValue, 0.7 );
		//fragmentColor = colorFresnel;

	//FOAM:
		vec4 foamColor = texture(foamTexture, finalTC_foam);
		vec4 colorFoam = mix(colorFresnel, foamColor, foamIntensity*scale(maxAmp, passPosWS.y));
		fragmentColor = colorFoam;
		fragmentColor.a = 1.0;

	//LIGHTING:
		vec3 lightDirection = (lightPos - passPos.xyz);
		vec3 lightColor = vec3(0.9);
		float mat = 0.75;

	vec3 L = normalize(-lightDirection);
	vec3 E = normalize(passSightVec);
	vec3 R = reflect(-L, normalize(normalWS));

	float specular = pow( max( dot(R, E), 0.0), shineFactor );
	vec3 specularLight = mat * lightColor * specular;

	fragmentColor.rgb += specularLight;

	//fragmentColor = vec4(tcTest.x, tcTest.y, 0, 1);
}
