#version 330

layout (location = 0) in vec4 Position;
layout (location = 1) in vec3 Normal;
layout (location = 2) in vec2 Tcoord;

const int MAX_NUMBER_OF_WAVES = 60;

struct Wave
{
	vec3  direction;
    float amplitude;
    float waveLength;
    float parameterK;
    float frequency;
    float phaseVelocity;
    float phaseShift;
};

uniform Wave waves[MAX_NUMBER_OF_WAVES];

uniform float timeStep;
uniform float oceanWidth;
uniform float oceanDepth;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

//Corner Points of ocean in WS
uniform vec4 bottomLeftPoint;
uniform vec4 bottomRightPoint;
uniform vec4 topLeftPoint;
uniform vec4 topRightPoint;

uniform vec3 eyePosWS;

out vec4 passPos;
out vec4 passPosWS;
out vec3 passNormal;
out vec3 passTangent;
out vec3 passBitangent;
out mat3 passNormalMatrix;

out vec2 passTC;
out vec3 passSightVec;

//  0 <----u----> 1
//  a ----------- b    0
//  |             |   /|\
//  |             |    |
//  |             |    v
//  |  *(u,v)     |    |
//  |             |   \|/
//  d------------ c    1

vec3 bilinearLerp(vec3 a, vec3 b, vec3 c, vec3 d, float u, float v)
{
   vec3 abu = mix(a, b, u);
   vec3 dcu = mix(d, c, u);
   return mix(abu, dcu, v);
}

float getCurrentDepth(vec3 a, vec3 b, vec3 c, vec3 d, float u)
{
	vec3 abu = mix(a, b, u);
	vec3 dcu = mix(d, c, u);
	return length(abu - dcu);
}

float getCurrentWidth(vec3 a, vec3 b, vec3 c, vec3 d, float v)
{
	vec3 adv = mix(a, d, v);
	vec3 bcv = mix(b, c, v);
	return length(bcv - adv);
}

vec3 calcGerstnerPos(vec3 origPos)
{
	vec3 newPos = vec3(0.0,0.0,0.0);

	for(int i = 0; i < MAX_NUMBER_OF_WAVES; i++)
	{

		newPos.x +=  waves[i].amplitude * waves[i].direction.x * sin(
			waves[i].parameterK * dot(waves[i].direction, origPos) -
			waves[i].frequency * timeStep * waves[i].phaseVelocity +
			waves[i].phaseShift
			);

		newPos.y += waves[i].amplitude * cos(
			waves[i].parameterK * dot(waves[i].direction, origPos) -
			waves[i].frequency * timeStep * waves[i].phaseVelocity +
			waves[i].phaseShift
			);

		newPos.z +=  waves[i].amplitude * waves[i].direction.z * sin(
			waves[i].parameterK * dot(waves[i].direction, origPos) -
			waves[i].frequency * timeStep * waves[i].phaseVelocity +
			waves[i].phaseShift
			);

	}

	newPos.x = origPos.x - newPos.x;
	newPos.z = origPos.z - newPos.z;
	return newPos;
}

void calcGerstnerTBN(vec3 origPos)
{
	//calc x and z derivatives
	float bX = 0.0;
	float bY = 0.0;
	float bZ = 0.0;

	float tX = 0.0;
	float tY = 0.0;
	float tZ = 0.0;

	for(int i = 0; i < MAX_NUMBER_OF_WAVES; i++)
	{
		float squaredDirX = waves[i].direction.x * waves[i].direction.x;
		float squaredDirZ = waves[i].direction.z * waves[i].direction.z;

		bX += waves[i].amplitude * squaredDirX * waves[i].parameterK * cos(
			waves[i].parameterK * dot(waves[i].direction, origPos) -
			waves[i].frequency * timeStep * waves[i].phaseVelocity +
			waves[i].phaseShift);

		bY += waves[i].amplitude * waves[i].direction.x * waves[i].parameterK * sin(
			waves[i].parameterK * dot(waves[i].direction, origPos) -
			waves[i].frequency * timeStep * waves[i].phaseVelocity +
			waves[i].phaseShift);

		bZ += waves[i].amplitude * waves[i].direction.x * waves[i].direction.z * waves[i].parameterK * cos(
			waves[i].parameterK * dot(waves[i].direction, origPos) -
			waves[i].frequency * timeStep * waves[i].phaseVelocity +
			waves[i].phaseShift);

		tX += waves[i].amplitude * waves[i].direction.x * waves[i].direction.z * waves[i].parameterK * cos(
			waves[i].parameterK * dot(waves[i].direction, origPos) -
			waves[i].frequency * timeStep * waves[i].phaseVelocity +
			waves[i].phaseShift);

		tY += waves[i].amplitude * waves[i].direction.z * waves[i].parameterK * sin(
			waves[i].parameterK * dot(waves[i].direction, origPos) -
			waves[i].frequency * timeStep * waves[i].phaseVelocity +
			waves[i].phaseShift);

		tZ += waves[i].amplitude * squaredDirZ * waves[i].parameterK * cos(
			waves[i].parameterK * dot(waves[i].direction, origPos) -
			waves[i].frequency * timeStep * waves[i].phaseVelocity +
			waves[i].phaseShift);

	}

	passBitangent	= normalize(vec3(1 - bX, -bY, -bZ));
	passTangent	= normalize(vec3(-tX, -tY, 1 - tZ));
	passNormal = normalize(cross(passTangent,passBitangent));
}

void main()
{
	vec4 projectedGridPos = vec4(bilinearLerp(
			topLeftPoint.xyz,
			topRightPoint.xyz,
			bottomRightPoint.xyz,
			bottomLeftPoint.xyz,
			Position.x,
			Position.z),
			1.0
		 );

	vec4 gerstnerPos = vec4(calcGerstnerPos(projectedGridPos.xyz),1.0);
	calcGerstnerTBN(projectedGridPos.xyz);

	gl_Position = projectionMatrix * viewMatrix * modelMatrix * gerstnerPos;
	passPosWS = gerstnerPos;
	passPos = projectedGridPos * projectionMatrix * viewMatrix * modelMatrix;

	passNormalMatrix = mat3( transpose( inverse( viewMatrix * modelMatrix)));

	vec2 texCoordScaleFactor;

	float currentDistanceX = getCurrentWidth(topLeftPoint.xyz,
			topRightPoint.xyz,
			bottomRightPoint.xyz,
			bottomLeftPoint.xyz,
			1.0 - Position.z);

	float currentDistanceZ = getCurrentDepth(topLeftPoint.xyz,
			topRightPoint.xyz,
			bottomRightPoint.xyz,
			bottomLeftPoint.xyz,
			Position.x);

	float testDistZ = length(topRightPoint - bottomRightPoint);
	float testDistX = length(topRightPoint - topLeftPoint);

	passTC.x = mod(Tcoord.x * currentDistanceX, 1.0);
	passTC.y = mod(Tcoord.y * currentDistanceZ, 1.0);

	passSightVec = normalize( vec3(projectedGridPos) - eyePosWS );
}
