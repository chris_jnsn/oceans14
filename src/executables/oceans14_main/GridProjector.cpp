#include "GridProjector.h"
#include "OceanCamera.h"

//IDEA: create perfect desired post-perspective grid and run it backwards through the pipeline to find out world positions of that perfect grid

const unsigned int FRUSTUM_LINES_COUNT = 8;

const glm::vec2 FRUSTUM_LINE_INDICES[FRUSTUM_LINES_COUNT] = 
{
        //near plane
        //glm::vec2(0,1),   //always parallel
        glm::vec2(1,2),
        //glm::vec2(2,3),   //always parallel
        glm::vec2(3,0),  

        //far plane
        //glm::vec2(4,5),   //always parallel
        glm::vec2(5,6),
        //glm::vec2(6,7),   //always parallel
        glm::vec2(7,4),

        //frustum lines CW starting left top
        glm::vec2(0,4),
        glm::vec2(1,5),
        glm::vec2(2,6),
        glm::vec2(3,7)
};

GridProjector::GridProjector(OceanCamera * oceanCam) : m_oceanCamera(oceanCam), m_frustumOceanIntersectionsWS(new std::vector<glm::vec3>()), m_xzRangePoints(std::array<glm::vec4, 4>())
{
    m_xzRangePoints[0] = glm::vec4(0, 0, 0, 0); //xMin
    m_xzRangePoints[1] = glm::vec4(0, 0, 0, 0); //xMax
    m_xzRangePoints[2] = glm::vec4(0, 0, 0, 0); //zMin 
    m_xzRangePoints[3] = glm::vec4(0, 0, 0, 0); //zMax
}

GridProjector::~GridProjector()
{
    delete m_frustumOceanIntersectionsWS;   
}

bool GridProjector::updateGrid(CVK::Geometry & geom, float const & maxAmplitude, float const & baseHeight)
{
    //determine if any part of the displacable volume is within the camera frustum
    calculateFrustumOceanIntersections(maxAmplitude,baseHeight);
    
    //Tell Wave Simulation not to update the ocean when no intersection points were found
    if (m_frustumOceanIntersectionsWS->size() < 3)
       return false;

    //project frustum ocean intersections on base ocean plane
    std::vector<glm::vec4> projectedFrustumOceanIntersectionsWS;
    for (int i = 0; i < m_frustumOceanIntersectionsWS->size(); i++)
        projectedFrustumOceanIntersectionsWS.push_back(glm::vec4(
            m_frustumOceanIntersectionsWS->at(i).x,
            baseHeight,
            m_frustumOceanIntersectionsWS->at(i).z,
            1.0)
            );
    
   
    //aim projector

    //calculate projector ocean point
    calculateProjectorPoint(baseHeight);

    //projector position - keep projector above S_upper at all times
    glm::vec3 P_position = m_oceanCamera->getCamPos();
    if (P_position.y < baseHeight + maxAmplitude)
        P_position.y = baseHeight + maxAmplitude;

    //perspective inherited from rendering camera
    glm::mat4 M_perspective = *m_oceanCamera->getProjection()->getProjMatrix();

    //create view through ocean projector
    glm::mat4 M_pview = *m_oceanCamera->getView();
    //glm::mat4 M_pview = glm::lookAt(P_position, m_projectorPoint, *m_oceanCamera->getUpVector());

    //create final projector matrix
    glm::mat4 M_projector = M_perspective * M_pview;

    //project frustum ocean intersections into normalized devide coordinates
    std::vector<glm::vec4> projectedFrustumOceanIntersectionsNDC;
    for (int i = 0; i < projectedFrustumOceanIntersectionsWS.size(); i++)
    {
        glm::vec4 intersectionCS    = M_projector * projectedFrustumOceanIntersectionsWS.at(i);

        //persp. division
        glm::vec4 intersectionNDC   = intersectionCS / intersectionCS.w;

        //safety margin to prevent backfiring
        if (intersectionNDC.y <= 1.0 && intersectionNDC.y >= -1.0 &&
            intersectionNDC.z <= 1.0 && intersectionNDC.z > -1.0
            )
            projectedFrustumOceanIntersectionsNDC.push_back(intersectionNDC);
    }

    //find out min and max x,y values from the cameras pov
    float x_minNDC, x_maxNDC, y_minNDC, y_maxNDC;

    for (std::vector<glm::vec4>::iterator it = projectedFrustumOceanIntersectionsNDC.begin(); it != projectedFrustumOceanIntersectionsNDC.end(); ++it)
    {
        
        if (it == projectedFrustumOceanIntersectionsNDC.begin())
        {
                x_minNDC = (*it).x;
                x_maxNDC = (*it).x;
                y_minNDC = (*it).y;
                y_maxNDC = (*it).y;
        }

        else
        {
                if ((*it).x < x_minNDC)
                    x_minNDC = (*it).x;
                else if ((*it).x > x_maxNDC)
                    x_maxNDC = (*it).x;
                if ((*it).y < y_minNDC)
                    y_minNDC = (*it).y;
                else if ((*it).y > y_maxNDC)
                    y_maxNDC = (*it).y;
        }
    }

    //from min and max values create the corner points of the ocean once for negative z value and once for positive z value (to get z range)
    glm::vec4 cornerPointsNDC_posZ[4];
    glm::vec4 cornerPointsNDC_negZ[4];

    //add safety margin
    float safetyMargin = 0.4f;

    x_minNDC -= safetyMargin;
    y_minNDC -= safetyMargin;
    y_maxNDC += 0.07;
    x_maxNDC += safetyMargin;

    //y_maxNDC doesnt need a safety margin

    cornerPointsNDC_posZ[0] = glm::vec4(x_minNDC,y_minNDC, 1, 1);
    cornerPointsNDC_posZ[1] = glm::vec4(x_maxNDC,y_minNDC, 1, 1);
    cornerPointsNDC_posZ[2] = glm::vec4(x_minNDC,y_maxNDC, 1, 1);
    cornerPointsNDC_posZ[3] = glm::vec4(x_maxNDC,y_maxNDC, 1, 1);

    cornerPointsNDC_negZ[0] = glm::vec4(x_minNDC,y_minNDC,-1, 1);
    cornerPointsNDC_negZ[1] = glm::vec4(x_maxNDC,y_minNDC,-1, 1);
    cornerPointsNDC_negZ[2] = glm::vec4(x_minNDC,y_maxNDC,-1, 1);
    cornerPointsNDC_negZ[3] = glm::vec4(x_maxNDC,y_maxNDC,-1, 1);

    //Transform corner points back into WS
    glm::vec4 cornerPointsWS_posZ[4];
    glm::vec4 cornerPointsWS_negZ[4];

    //use inverse projector matrix to do that
    glm::mat4 M_projectorInv = glm::inverse(M_projector);
    for(int i = 0; i < 4; i++)
    {
        cornerPointsWS_posZ[i] = M_projectorInv * cornerPointsNDC_posZ[i];
        cornerPointsWS_negZ[i] = M_projectorInv * cornerPointsNDC_negZ[i];

        //persp. division
        cornerPointsWS_posZ[i] = cornerPointsWS_posZ[i]/cornerPointsWS_posZ[i].w;
        cornerPointsWS_negZ[i] = cornerPointsWS_negZ[i]/cornerPointsWS_negZ[i].w;
    }

    //Find out final corner points in WS
    glm::vec4 finalCornerPointsWS[4];
    glm::vec4 planeNormal = glm::vec4(0,1,0,1);
    for(int i = 0; i < 4; i++)
    {
        //find intersection of line between pos and neg. z value and the ocean plane
        glm::vec4 currentEdgeDirection = glm::normalize(glm::vec4(cornerPointsWS_posZ[i] - cornerPointsWS_negZ[i]));

        float dotDirectionNormal = glm::dot(currentEdgeDirection,planeNormal);

        if (dotDirectionNormal == 0.0f)
        {
            std::cout << "ERROR IN GRID PROJECTOR" << std::endl;
            m_xzRangePoints[i] = glm::vec4(0,0,0,0);
        }
        else
        {
            float d = glm::dot((glm::vec4(0,0,0,1) - cornerPointsWS_negZ[i]),planeNormal) / dotDirectionNormal;
            m_xzRangePoints[i] = d * currentEdgeDirection + cornerPointsWS_negZ[i];
        }
    }


    //Tell Wave Simulation to update the ocean
    return true;
}

void GridProjector::calculateFrustumOceanIntersections(float const & maxAmplitude, float const & baseHeight)
{
    m_frustumOceanIntersectionsWS->clear();

    if (m_oceanCamera->getFrustumCornersWS().size() == 0)
        std::cout << "Error in GridProjector::getFrustumIntersections - ocean camera frustum corners in WS is empty!" << std::endl;
   
    //define point on lower and upper bound plane (dependant on max amplitude)
    glm::vec3 pointOnLowerPlane = glm::vec3(0,baseHeight - maxAmplitude,0);
    glm::vec3 pointOnUpperPlane = glm::vec3(0,baseHeight + maxAmplitude,0);

    glm::vec3 planeNormal = glm::vec3(0.0f,1.0f,0.0f);

    //loop over 12 lines of the frustum and check for intersections with the upper and lower plane
    for (int i = 0; i < FRUSTUM_LINES_COUNT; i++)
    {

        glm::vec3 currentEdgePointA = glm::vec3(m_oceanCamera->getFrustumCornersWS()[FRUSTUM_LINE_INDICES[i].x]);
        glm::vec3 currentEdgePointB = glm::vec3(m_oceanCamera->getFrustumCornersWS()[FRUSTUM_LINE_INDICES[i].y]);

        glm::vec3 currentEdgeDirection = glm::normalize(glm::vec3(currentEdgePointB - currentEdgePointA));

        float dotDirectionNormal = glm::dot(currentEdgeDirection,planeNormal);

        //No intersection if ocean and current line are parallel
            //Dot product between two vectors with a angle of 90° gets 0
            //therefore if normal and current direction are perpendicular,
            //ocean and current line have to be parallel to each other and no
            //intersection can be found
        if (dotDirectionNormal == 0.0f)
            continue;

        //Check upper and lower plane for intersection
        float dLower = glm::dot((pointOnLowerPlane - currentEdgePointA),planeNormal) / dotDirectionNormal;
        glm::vec3 newIntersectionLower = dLower * currentEdgeDirection + currentEdgePointA;

        float dUpper = glm::dot((pointOnUpperPlane - currentEdgePointA),planeNormal) / dotDirectionNormal;
        glm::vec3 newIntersectionUpper = dUpper * currentEdgeDirection + currentEdgePointA;

        //save intersections
        m_frustumOceanIntersectionsWS->push_back(newIntersectionLower);
        m_frustumOceanIntersectionsWS->push_back(newIntersectionUpper);
    }
}

void GridProjector::calculateProjectorPoint(float const & baseHeight)
{
    glm::vec3 C_view = m_oceanCamera->getCamDirection();
    glm::vec3 oceanNormal = glm::vec3(0,1,0);

    //check if camera is aiming to the ocean
    //if yes, take method one for point generation
    //otherwise use method two
    float dotViewNorm = glm::dot(C_view, oceanNormal);
    if (dotViewNorm < 0)
    {
        //intersect view vector with ocean plane
        float d = glm::dot((glm::vec3(0, baseHeight, 0) - m_oceanCamera->getCamPos()), oceanNormal) / dotViewNorm;
        m_projectorPoint = d * C_view + m_oceanCamera->getCamPos();
    }

    else
    {
        //calculate point at fixed distance in view direction
        //fixed distance is dependant on frustum size (calculating vector from Near to Far plane left top corners and taking a third of that
        glm::vec3 vecLeftCornerNearFar = glm::vec3(m_oceanCamera->getFrustumCornersWS()[4] - m_oceanCamera->getFrustumCornersWS()[0]);
        float vecLeftCornerNearFarLength = glm::length(vecLeftCornerNearFar);

        m_projectorPoint = m_oceanCamera->getCamPos() + m_oceanCamera->getCamDirection() * vecLeftCornerNearFarLength / 3.0f;
        
        //project point onto ocean base
        m_projectorPoint.y = baseHeight;
    }
}