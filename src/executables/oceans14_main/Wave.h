#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

class Wave
{
public:
    Wave();
    Wave(glm::vec3 const & direction, float const & amplitude, float const & waveLength, float const & phaseShift);
    ~Wave();

    inline glm::vec3 const & getDirection()     { return m_direction; }
    inline float const & getAmplitude()         { return m_amplitude; }
    inline float const & getWaveLength()        { return m_waveLength; }
    inline float const & getParameterK()        { return m_parameterK; }
    inline float const & getFrequency()         { return m_frequency; }
    inline float const & getPhaseVelocity()     { return m_phaseVelocity; }
    inline float const & getPhaseShift()        { return m_phaseShift; }

    inline void setDirection(glm::vec3 const & direction)   { m_direction = direction; }
    inline void setAmplitude(float const & amplitude)       { m_amplitude = amplitude; }
    inline void setPhaseShift(float const & phaseShift)     { m_phaseShift = phaseShift; }
    
    //wavelength influences multiple parameters - therefore there are not special setters for those params
    void setWaveLength(float const & waveLength);

protected:

private:

    glm::vec3   m_direction;
    float       m_amplitude;
    float       m_waveLength;
    float       m_parameterK;
    float       m_frequency;
    float       m_phaseVelocity;
    float       m_phaseShift;
};