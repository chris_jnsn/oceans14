#include "Noise.h"

#include <iostream>
#include "ShaderMapGenerator.h"
#include "ShaderNoiseGenerator.h"

Noise::Noise(unsigned int const & noiseMapWidth, unsigned int const & noiseMapHeight) :

	m_noisemapFBO(nullptr),
	m_normalmapFBO(nullptr),
	m_noiseShader(nullptr),
	m_normalShader(nullptr),
	m_sfqOrtho(new CVK::Ortho(0.0f, 1.0f, 0.0f, 1.0f, 0.1f, 100.0f))

{
	initShader();

	m_noisemapFBO = new CVK::FBO(noiseMapWidth, noiseMapHeight, 1);
	m_normalmapFBO = new CVK::FBO(noiseMapWidth, noiseMapHeight, 1);

	m_sfqCamera = new CVK::Trackball(noiseMapWidth, noiseMapHeight, m_sfqOrtho);

	m_sfqPlane = new CVK::Plane();
	m_sfqPlane->set_Points(
		glm::vec3(-1.f, 1.f, 0.f),
		glm::vec3(-1.f, -1.f, 0.f),
		glm::vec3(1.f, -1.f, 0.f),
		glm::vec3(1.f, 1.f, 0.f));

	m_sfqPlane->set_Tcoords(
		glm::vec2(0.f, 1.f),
		glm::vec2(0.f, 0.f),
		glm::vec2(1.f, 0.f),
		glm::vec2(1.f, 1.f)
		);

	m_sfqNode = new CVK::Node("sfq");
	m_sfqNode->setGeometry(m_sfqPlane);
}

Noise::~Noise()
{    
	delete m_noisemapFBO;
	delete m_normalmapFBO;
	delete m_sfqCamera;
	delete m_sfqNode;
	delete m_sfqPlane;

}


void Noise::update(float const & timeStep)
{
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);

	CVK::State::getInstance()->setCamera(m_sfqCamera);

	//UPDATE NOISEMAP:
	m_noisemapFBO->bind();
    glClear(GL_COLOR_BUFFER_BIT);

	CVK::State::getInstance()->setShader(m_noiseShader);
	m_noiseShader->update(timeStep);

    m_sfqNode->render();
	m_noisemapFBO->unbind();  

    /*
	//UPDATE NORMALMAP:
	m_normalmapFBO->bind();
	glClear(GL_COLOR_BUFFER_BIT);

	CVK::State::getInstance()->setShader(m_normalShader);
	m_normalShader->update(*m_noisemapFBO);

	m_sfqNode->render();
	m_normalmapFBO->unbind();
    */
}

void Noise::initShader()
{
	const char *shaderNoise[2] = { SHADERS_PATH "/OceanRendering/SimplexNoise.vert", SHADERS_PATH "/OceanRendering/SimplexNoise.frag" };

	m_noiseShader = new ShaderNoiseGenerator(VERTEX_SHADER_BIT | FRAGMENT_SHADER_BIT, shaderNoise);

    const char *shaderNormal[2] = { SHADERS_PATH "/OceanRendering/NormalNoiseMap.vert", SHADERS_PATH "/OceanRendering/NormalNoiseMap.frag" };

    m_normalShader = new ShaderMapGenerator(VERTEX_SHADER_BIT | FRAGMENT_SHADER_BIT, shaderNormal);
}
