#pragma once

#include "CVK_2/CVK_Defs.h"
#include "CVK_2/CVK_ShaderMinimal.h"

class Wave;

class ShaderOceanSimulationSinus : public CVK::ShaderMinimal
{
public:
    ShaderOceanSimulationSinus(GLuint shader_mask, const char** shaderPaths);
    void update(unsigned int const & oceanWidth, unsigned int const & oceanHeight, float const & timeStep, std::vector<Wave*> const & waves);

private:

	GLuint m_oceanWidthID;
    GLuint m_oceanHeightID;
    GLuint m_timeStepID;
};
