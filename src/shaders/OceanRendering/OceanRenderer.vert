#version 330

layout (location = 0) in vec4 Position;
layout (location = 1) in vec3 Normal;
layout (location = 2) in vec2 Tcoord;

uniform sampler2D heightMap;
uniform sampler2D normalMap;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

//Corner Points of ocean in WS
uniform vec4 bottomLeftPoint;
uniform vec4 bottomRightPoint;
uniform vec4 topLeftPoint;
uniform vec4 topRightPoint;

uniform vec3 eyePosWS;

out vec4 passPos;
out vec3 passNorm;
out vec2 passTC;
out vec3 sightVec;


//  0 <----u----> 1
//  a ----------- b    0
//  |             |   /|\
//  |             |    |
//  |             |    v
//  |  *(u,v)     |    |
//  |             |   \|/
//  d------------ c    1

vec3 bilinearLerp(vec3 a, vec3 b, vec3 c, vec3 d, float u, float v)
{
   vec3 abu = mix(a, b, u);
   vec3 dcu = mix(d, c, u);
   return mix(abu, dcu, v);
}

void main() {

	vec4 finalPos = vec4(bilinearLerp(
		topLeftPoint.xyz,
		topRightPoint.xyz,
		bottomRightPoint.xyz,
		bottomLeftPoint.xyz,
		Position.x,
		1.0 - Position.z),
		1.0
	 );

	gl_Position = projectionMatrix * viewMatrix * modelMatrix * finalPos;
	passPos = finalPos;

	mat3 normalMatrix = mat3( transpose( inverse( viewMatrix * modelMatrix)));
	passNorm = normalize( normalMatrix * Normal);
	passTC = Tcoord;

	sightVec = normalize( vec3(Position) - eyePosWS );
}
