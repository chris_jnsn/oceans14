#version 330 core

in vec3 passPosition;
in vec2 passTcoord;

uniform sampler2D noiseMap;

out vec4 fragmentColor;

vec3 calculateNormal()
{
	float  heightCurr = texture (noiseMap, passTcoord).x;
	float  heightTop = texture (noiseMap, passTcoord + passTcoord * vec2( 0, -1)).x;
	float  heightRight = texture (noiseMap, passTcoord + passTcoord * vec2( 1,  0)).x;

	//calc normal and bring to range 0..1
	vec3 norm = vec3
	(
	 ((heightCurr - heightTop) + 1) / 2.0,
	 ((heightCurr - heightRight) + 1) / 2.0,
	 1.0
	);

	return normalize(norm);
}



void main()
{
  fragmentColor = vec4(calculateNormal(),1.0);
}
