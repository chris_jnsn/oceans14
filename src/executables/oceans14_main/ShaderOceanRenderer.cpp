#include "ShaderOceanRenderer.h"


ShaderOceanRenderer::ShaderOceanRenderer(GLuint shader_mask, const char** shaderPaths, CVK::CubeMapTexture *cubeMap, CVK::Texture *texture) : CVK::ShaderMinimal(shader_mask,shaderPaths)
{
	//Material
	m_kdID = glGetUniformLocation( m_ProgramID, "mat.kd");
	m_ksID = glGetUniformLocation( m_ProgramID, "mat.ks");
	m_ktID = glGetUniformLocation( m_ProgramID, "mat.kt");

	m_diffuseID = glGetUniformLocation( m_ProgramID, "mat.diffColor"); 
	m_specularID = glGetUniformLocation( m_ProgramID, "mat.specColor"); 
	m_shininessID = glGetUniformLocation( m_ProgramID, "mat.shininess");

	m_camPos = glGetUniformLocation(m_ProgramID, "eyePosW");

	m_foamTextureID = glGetUniformLocation(m_ProgramID, "foamTexture");
	m_foamTexture = texture;

	m_noiseMapID = glGetUniformLocation(m_ProgramID, "noiseMap");

	//Light
	m_numLightsID = glGetUniformLocation( m_ProgramID, "numLights");
	for (unsigned int i = 0 ; i < MAX_LIGHTS; i++)
	{
		char string[100];
		sprintf( string, "light[%d].pos", i);
		m_lightposID[i] = glGetUniformLocation( m_ProgramID, string);
		sprintf( string, "light[%d].col", i);
		m_lightcolID[i] = glGetUniformLocation( m_ProgramID, string); 
		sprintf( string, "light[%d].spot_direction", i);
		m_lightsdirID[i] = glGetUniformLocation( m_ProgramID, string);
		sprintf( string, "light[%d].spot_exponent", i);
		m_lightsexpID[i] = glGetUniformLocation( m_ProgramID, string);
		sprintf( string, "light[%d].spot_cutoff", i);
		m_lightscutID[i] = glGetUniformLocation( m_ProgramID, string);
	}

	//Textures
	m_useColorTexture = glGetUniformLocation( m_ProgramID, "useColorTexture");
	m_colorTextureID = glGetUniformLocation( m_ProgramID, "colorTexture");
    
    m_heightMapID = glGetUniformLocation(m_ProgramID, "heightMap");
    m_normalMapID = glGetUniformLocation( m_ProgramID, "normalMap");

    m_bottomLeftPointID = glGetUniformLocation(m_ProgramID, "bottomLeftPoint");
    m_bottomRightPointID = glGetUniformLocation(m_ProgramID, "bottomRightPoint");
    m_topLeftPointID = glGetUniformLocation(m_ProgramID, "topLeftPoint");
    m_topRightPointID = glGetUniformLocation(m_ProgramID, "topRightPoint");

    m_showNormalsID = glGetUniformLocation(m_ProgramID, "showNormals");
	m_showReflectionsID = glGetUniformLocation(m_ProgramID, "showReflections");
	m_showSunID = glGetUniformLocation(m_ProgramID, "showSun");
	m_showFoamID = glGetUniformLocation(m_ProgramID, "showFoam");
	m_shineFactorID = glGetUniformLocation(m_ProgramID, "shineFactor");
	m_foamIntensityID = glGetUniformLocation(m_ProgramID, "foamIntensity");

	m_cubeMapTexture = cubeMap;
	m_cubeMapTextureID = glGetUniformLocation(m_ProgramID, "ReflectionMap");
}

void ShaderOceanRenderer::update(CVK::FBO & heightMapFBO, std::array<glm::vec4,4> const & xzRangePoints, glm::vec3 *camPosition, bool showNormals, bool showReflections, bool showSun, bool showFoam, float shininess, float foamIntensity)
{
	int numLights = CVK::State::getInstance()->getLights()->size();
	CVK::ShaderMinimal::update();

	glActiveTexture(GL_TEXTURE0);
  	glBindTexture(GL_TEXTURE_2D, heightMapFBO.getColorTexture(0));
  	glUniform1i(m_heightMapID, 0);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, heightMapFBO.getColorTexture(1));
    glUniform1i(m_normalMapID, 1);

    glUniform4fv(m_bottomLeftPointID, 1, glm::value_ptr(xzRangePoints[0]));
    glUniform4fv(m_bottomRightPointID, 1, glm::value_ptr(xzRangePoints[1]));
    glUniform4fv(m_topLeftPointID, 1, glm::value_ptr(xzRangePoints[2]));
    glUniform4fv(m_topRightPointID, 1, glm::value_ptr(xzRangePoints[3]));
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, heightMapFBO.getColorTexture(2));
	glUniform1i(m_noiseMapID, 3);

	glUniform3fv(m_camPos, 1, glm::value_ptr(*camPosition));

	glUniform1i( m_numLightsID, numLights);
	for (int i = 0 ; i < numLights; i++)
	{
		CVK::Light *light = &CVK::State::getInstance()->getLights()->at(i);
		glUniform4fv( m_lightposID[i], 1, glm::value_ptr( *light->getPosition()));
		glUniform3fv( m_lightcolID[i], 1, glm::value_ptr( *light->getColor()));
		glUniform3fv( m_lightsdirID[i], 1, glm::value_ptr( *light->getSpotDirection()));
		glUniform1f( m_lightsexpID[i], light->getSpotExponent());
		glUniform1f( m_lightscutID[i], light->getSpotCutoff());
	}

    glUniform1f(m_showNormalsID, showNormals);
	glUniform1f(m_showReflectionsID, showReflections);
	glUniform1f(m_showSunID, showSun);
	glUniform1f(m_showFoamID, showFoam);
	glUniform1f(m_shineFactorID, shininess);
	glUniform1f(m_foamIntensityID, foamIntensity);

	glUniform3fv( m_lightambID, 1, glm::value_ptr( CVK::State::getInstance()->getLightAmbient()));

	if (m_cubeMapTexture)
	{
		glUniform1i(m_cubeMapTextureID, 0);
		glActiveTexture(CUBE_MAP_TEXTURE_UNIT);
		m_cubeMapTexture->bind();
	}

	if (m_foamTexture)
	{
		glUniform1i(m_foamTextureID, 4);
		glActiveTexture(GL_TEXTURE4);
		m_foamTexture->bind();
		
	}
}

void ShaderOceanRenderer::update( CVK::Node* node)
{

	if( node->hasMaterial())
	{
		CVK::Material* mat = node->getMaterial();
		CVK::Texture *color_texture;

		glUniform1f( m_kdID, mat->getKd());
		glUniform1f( m_ksID, mat->getKs());
		glUniform1f( m_ktID, mat->getKt());
		glUniform3fv( m_diffuseID, 1, glm::value_ptr( *mat->getdiffColor()));
		glUniform3fv( m_specularID, 1, glm::value_ptr( *mat->getspecColor()));
		glUniform1f( m_shininessID, mat->getShininess());

		bool colorTexture = mat->hasTexture(CVK::COLOR_TEXTURE);
		glUniform1i( m_useColorTexture, colorTexture);

		if (colorTexture)
		{	
			glUniform1i( m_colorTextureID, 2);

			glActiveTexture(COLOR_TEXTURE_UNIT + 2);
			color_texture = mat->getTexture(CVK::COLOR_TEXTURE);
			color_texture->bind();
		}

	}
}
