#pragma once

#include "CVK_2/CVK_Defs.h"
#include "CVK_2/CVK_ShaderMinimal.h"

class ShaderSimpleTexture : public CVK::ShaderMinimal
{

public:
    ShaderSimpleTexture(GLuint shader_mask, const char** shaderPaths);
    virtual void update(GLuint texture);

private:
    GLuint m_colorTextureID;
};
