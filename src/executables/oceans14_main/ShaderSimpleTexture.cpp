#include "ShaderSimpleTexture.h"


ShaderSimpleTexture::ShaderSimpleTexture(GLuint shader_mask, const char** shaderPaths) : CVK::ShaderMinimal(shader_mask, shaderPaths)
{
    m_colorTextureID = glGetUniformLocation(m_ProgramID, "colortexture");
}

void ShaderSimpleTexture::update(GLuint texture)
{
    if (m_textures.size() > 0)
    {
        glUniform1i(texture, 0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_textures[0]);
    }
}
