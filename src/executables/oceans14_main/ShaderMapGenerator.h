#pragma once

#include "CVK_2/CVK_Defs.h"
#include "CVK_2/CVK_ShaderMinimal.h"
#include "CVK_2/CVK_FBO.h"

class ShaderMapGenerator : public CVK::ShaderMinimal
{
public:
	ShaderMapGenerator( GLuint shader_mask, const char** shaderPaths);
	void update( CVK::FBO & noiseFBO);

private:
	GLuint m_noiseTextureID;
};
