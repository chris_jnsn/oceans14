# oceans14
Real time ocean rendering as an assignment for the lecture real time rendering of the University Koblenz-Landau
 
=======================================================================================================================	
## Sources:
GPU gems chapter 1&2

	http://http.developer.nvidia.com/GPUGems/gpugems_ch01.html
	http://http.developer.nvidia.com/GPUGems/gpugems_ch02.html


Siggraph 2013 Slides

	advances.realtimerendering.com/s2013/OceanShoestring_SIGGRAPH2013_Online.pptx


Shader Toy Ocean Shader

	https://www.shadertoy.com/view/Ms2SD1

=======================================================================================================================	
**Build on Windows**

- Create project files with Cmake and build inside your respective IDE
- For information on Release Build go to the bottom of this page

=======================================================================================================================

**Build on Linux**

- Install some standard opengl packages:

  > $ sudo apt-get install xorg-dev

  > $ sudo apt-get install libglu1-mesa-dev

- Get Assimp and compile it:

  > $ git clone git://github.com/assimp/assimp.git assimp 

  > $ sudo apt-get install libboost-dev 

  > $ sudo apt-get install zlib1g-dev 

  > $ cd assimp 

  > $ cmake -G 'Unix Makefiles' 

  > $ make 

  > $ sudo make install 

  > $ sudo ldconfig

- Create project files with Cmake and build inside your respective IDE

- For information on Release Build go to the bottom of this page

=======================================================================================================================	

**Build on Windows**

- Go to ./dependencies/ and unzip the apple.zip file - new folder name should simply be "apple"
- Create project files with Cmake and build inside your respective IDE
- For information on Release Build go to the bottom of this page

=======================================================================================================================	

## Information on RELEASE BUILD:

For a Release BUILD do the following:
	
	- In Cmake check option RELEASE_BUILD
	- compile in your IDE (textures, shaders etc.. are copied to binary location)
		- In Visual Studio additionaly choose "Release" on top before compiling
	- The release versions of external dependencies are copied to the binary location automatically.
	- Start the *.exe file directly from the binary folder!!! 
	- Starting from inside of Visual Studio is not possible in release build, since resources arent found 
	- To return to a DEBUG build, uncheck RELEASE_BUILD in cmake again and build in your IDE
		- here again - choose "Debug" in Visual Studio
