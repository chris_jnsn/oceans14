#version 330
layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

uniform sampler2D heightMap;
uniform sampler2D normalMap;
uniform sampler2D noiseMap;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

in vec4 passPos[];
in vec3 passNorm[];
in vec3 sightVec[];
in vec2 passTC[];

out vec3 passPosition;
out vec3 passNormal;
out vec2 passTcoord;
out vec3 passPerlinNoiseWorld;
out vec3 passPerlinNoise;
out vec3 incident;
out vec3 normalWorld;
out float height;

vec3 calculateNormal(int index, vec3 currentPos)
{
	int indexA = int(mod(index+1,3.0));
	int indexB = int(mod(index+2,3.0));

	vec3 neighbourPosA	= texture(heightMap,passTC[indexA]).xyz;
	vec3 neighbourPosB	= texture(heightMap,passTC[indexB]).xyz;

	vec3 vecA = neighbourPosA - currentPos;
	vec3 vecB = neighbourPosB - currentPos;

	return normalize(cross(vecA,vecB));
}

void main()
{
	///TODO: calc. ocean matrix in a different way
	mat4 oceanMatrix = projectionMatrix * viewMatrix * modelMatrix;
	mat4 normalMatrix = transpose(inverse(viewMatrix * modelMatrix));

  	for(int i=0; i<3; i++)
  	{
		//vec4 newPos = texture(heightMap,passPos[i].xz);
		vec4 newPos = passPos[i];

		//x and z are zero in case of SINUS waves - so nothing happens there
		newPos.x = newPos.x - texture(heightMap,passPos[i].xz).x;
		newPos.y = texture(heightMap,passPos[i].xz).y;
		newPos.z = newPos.z - texture(heightMap,passPos[i].xz).z;

		vec4 newNorm = texture(normalMap,passPos[i].xz);
		vec4 newNoise = texture(noiseMap,passPos[i].xz);

		//in case of sinus waves normal was not calculated. Calculate it now via cross product
		if(newNorm == vec4(0.0,0.0,0.0,0.0))
			newNorm = vec4(calculateNormal(i,newPos.xyz),1.0);

		vec4 transformedPos = oceanMatrix * newPos;
    	gl_Position.xyzw = transformedPos.xyzw;
		passPosition = transformedPos.xyz;

		incident = sightVec[i];

		passTcoord = passTC[i];

		passPerlinNoiseWorld = normalize(newNoise).xyz;

		normalWorld = normalize(newNorm).xyz;

		height = newPos.y;

		passNormal = vec4(normalMatrix * newNorm).xyz;
		passNormal = normalize(passNormal);
		passPerlinNoise = vec4(normalMatrix * newNoise).xyz;
		passPerlinNoise = normalize(passPerlinNoise);
    	EmitVertex();
  	}
  	EndPrimitive();
}
