#pragma once

#include "CVK_2/CVK_Camera.h"
#include "CVK_2/CVK_Pilotview.h"
#include "CVK_2/CVK_Perspective.h"
#include "GUI.h"
#include <array>

class OceanCamera : public CVK::Pilotview
{
public:
    OceanCamera( int width, int height, CVK::Projection *projection, GUI * gui);
	~OceanCamera();

	void update( GLFWwindow *window, float const & maxAmpl);
    inline void setCamPos(glm::vec3 campos) { m_cameraPos = campos; }
    inline void setOceanBaseHeight(float const & oceanHeight) {m_oceanBaseHeight = oceanHeight;}
	inline glm::vec3 & getCamPos() { return m_cameraPos; }
    inline glm::vec3 getCamDirection() { return glm::normalize(m_direction); }
    
    inline float getNear() { return m_projection->getNear(); }
    inline float getFar() { return m_projection->getFar(); }
    inline float getFov() { return static_cast<CVK::Perspective*>(m_projection)->getFov(); }
    inline float getRatio() { return static_cast<CVK::Perspective*>(m_projection)->getRatio(); }
    inline glm::vec3 * getUpVector() { return &m_up; }

    inline std::array<glm::vec4, 8> & getFrustumCornersWS() { return m_frustumCornersWS; }

    void calculateFrustumCornersWS(float const & farPlaneToUse = -1.0);

private:

    GUI * m_gui;
    float m_oceanBaseHeight;
    std::array<glm::vec4, 8> m_frustumCornersWS;
};

