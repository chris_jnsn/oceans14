IF (MINGW)

	set(ASSIMP_INCLUDE_PATH "${CMAKE_SOURCE_DIR}/dependencies/assimp/include/")
	set(ASSIMP_LIB "${CMAKE_SOURCE_DIR}/dependencies/assimp/libMinGW/libassimp.a")

ELSEIF (MSVC)

    set(ASSIMP_INCLUDE_PATH "${CMAKE_SOURCE_DIR}/dependencies/assimp/include/")
    set(ASSIMP_LIB "${CMAKE_SOURCE_DIR}/dependencies/assimp/lib/assimp_release-dll_win32/assimp.lib")

ELSEIF("${CMAKE_SYSTEM}" MATCHES "Linux")
	
  FIND_PATH(ASSIMP_INCLUDE_PATH assimp/defs.h)
  FIND_LIBRARY(ASSIMP_LIBRARY
       NAMES assimp
	)
	
ELSEIF(APPLE)
	
	FIND_PATH(ASSIMP_INCLUDE_PATH assimp/defs.h
    ${CMAKE_SOURCE_DIR}/dependencies/assimp/include)
    
    FIND_LIBRARY( ASSIMP_LIBRARY
        NAMES libassimp.a
        PATHS ${CMAKE_SOURCE_DIR}/dependencies/apple/assimp/lib)
    SET(ASSIMP_LIB z ${ASSIMP_LIBRARY})

ENDIF ()


