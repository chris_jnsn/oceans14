#version 330 core

in vec3 passPosition;
in vec3 passNormal;
in vec2 passTcoord;

uniform samplerCube cubeTexture;

out vec4 fragmentColor;

void main() 
{ 
  fragmentColor = texture(cubeTexture, passPosition);
}