#pragma once

#include <CVK_2/CVK_Framework.h>
#include <array>

class OceanCamera;

class GridProjector
{
public:
    GridProjector(OceanCamera * oceanCam);
    ~GridProjector();

    bool updateGrid(CVK::Geometry & geom, float const & maxAmplitude, float const & baseHeight);

    inline std::vector<glm::vec3> & getFrustumOceanIntersectionsWS() {return *m_frustumOceanIntersectionsWS;}

    inline glm::vec3 & getProjectorPoint() { return m_projectorPoint; }

    inline std::array<glm::vec4, 4> const & getXZRangePoints() { return m_xzRangePoints; }

private:
    void calculateFrustumOceanIntersections(float const & maxAmplitude, float const & baseHeight);
    void calculateProjectorPoint(float const & baseHeight);

    OceanCamera * m_oceanCamera;
    std::vector<glm::vec3> * m_frustumOceanIntersectionsWS;
    glm::vec3 m_projectorPoint;
    std::array<glm::vec4, 4> m_xzRangePoints;
};