#pragma once

#include "CVK_2/CVK_Defs.h"
#include "CVK_2/CVK_ShaderMinimal.h"

class ShaderNoiseGenerator : public CVK::ShaderMinimal
{
public:
	ShaderNoiseGenerator(GLuint shader_mask, const char** shaderPaths);
    void update(float const & timeStep);

private:
    GLuint m_timeStepID;
};
